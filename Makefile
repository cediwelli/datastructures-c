CC = gcc
CFLAGS = -Wall -g3

CINCLUDES = -Iinclude/
CFILES = $(wildcard src/*.c)
COBJECTS = $(patsubst src/%.c,bin/%.o,$(CFILES))

TEST = tests
TESTOUT = $(TEST)/bin
TESTS = $(wildcard $(TEST)/*.c)
TESTSBIN = $(patsubst $(TEST)/%.c,$(TESTOUT)/%,$(TESTS))

COUT = bin/libdatastructures.so

LIBNAME = datastructures

all: $(COBJECTS)
	${CC} -fPIC $(COBJECTS) -shared -g3 -o $(COUT)
	chmod 644 $(COUT)

datastructures:	all

bin/%.o: src/%.c
	${CC} -c -g3 $< $(CINCLUDES) $(CFLAGS) -o $@

install:
	cp bin/lib$(LIBNAME).so /usr/lib/
	mkdir /usr/include/$(LIBNAME)
	cp $(wildcard include/*.h) $(wildcard include/*.hh) /usr/include/$(LIBNAME)

remove:
	rm /usr/lib/lib$(LIBNAME).so
	rm -r /usr/include/$(LIBNAME)
	
update:
	make
	sudo make remove
	sudo make install

testout:
	mkdir -p $(TESTOUT)

$(TESTOUT)/%: $(TEST)/%.c
	$(CC) $< -lcriterion -l$(LIBNAME) -o $@

tests: testout $(TESTSBIN)
	for test in $(TESTSBIN) ; do $$test --verbose=1 ; done
	
docs:
	doxygen doc/Doxyfile
