#ifndef ARRAY_H_
#define ARRAY_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup Array
 *
 * The Array Module allows for a variable-length array at runtime. It can
 * operate in two distinct modes.
 *
 * @see array
 */

/**
 * @ingroup Array
 * @brief
 * Template-ish way to announce what type the array has.
 * */
#define ARRAY(t) array *

/**
* @ingroup Array
* @brief
* The variable-length array will allocate memory at runtime automatically,\n
* without user intervention. It has several convenience functions like\n
* appending and inserting.\n
*\n
* REFERENCE MODE:\n
* The Array will only save the pointers to the inserted Elements.\n
* This means, the user must free the memory occupied by these Objects\n
* themself. This also means that an Object which was free'd becomes invalid\n
* and when dereferenced, there will most likely a segfault happen.\n
*\n
* VALUE MODE:\n
* The array will create a copy of the passed Elements and store those.\n
* This means, that the user can safely assume that any Element from the\n
* Array is a valid Object which can be dereferenced. This also means,\n
* that Element in the Array will be free'd automatically when the elements are\n
* deleted, removed or the Array is free'd.
*/
typedef struct array array;

/**
 * @ingroup Array
 * @brief
 * Defines a function interface for comparing data-objects. The function should
 * implement the following behaviour:
 * \n
 * IF obj1 has less value than obj2 (obj1 < obj2), the function shall return -1.
 * IF otherwise obj1 has more value (obj1 > obj2), the function shall return 1.
 * ELSE obj1 is equal to obj2 (obj1 == obj2), the function shall return 0.
 * \n
 * @note
 * If either of the objects is NULL, the function MUST return -2 as these are
 * considered invalid.
 * @param obj1 The first object, which will be compared against the second.
 * @param obj2 The second object, which will be compared against the first.
 * @return Returns 1 IF obj1>obj2, -1 IF obj1<obj2, 0 obj1==obj2 or -2 on Error.
 * */
typedef int (*array_comp_f)(void * obj1, void * obj2);

/**
 * @ingroup Array
 * @brief
 * Create a new Array, which will be dynamically allocated on the Heap.
 * This Array will automatically grow when appending new Data.
 *
 * To use the Reference-Mode, which means, not to copy the values
 * of the Pointers into the Array but just take the pointers as is,
 * pass 0 as data_size.
 *
 * @param[in] data_size The Size in Bytes one single Data-Element will take to store. Pass 0 to use Reference-Mode.
 * @return Returns a Pointer to the Array's Structure.
 * */
array * array_create(size_t data_size);

/**
 * @ingroup Array
 * @brief
 * When the User is done, the Array can be free'd an the Memory
 * can be given back to the OS by using this Function on the
 * Array-Pointer.
 *
 * @param[in] arr Pointer to the Array.
 * */
void array_free(array * arr);

/**
 * @ingroup Array
 * @brief
 * Enables the Comparison-Mode for the array which will make use of the passed
 * function, so that the Array is always sorted on each insertion.
 * These sorted insertions will be made by just the {@link array_addSorted}
 * Function. If the Array is in Comparison-Mode, it is not allowed to be
 * unsorted and therefor all of the other Functions that insert/add are
 * FORBIDDEN.
 *
 * @note
 * The user must ensure that the Array has no "holes" (NULL values), which are
 * caused by {@link array_delete deleting} an entry but not
 * {@link array_remove removing} it. Having holes in the Array will probably
 * cause it to not insert any Data, at some point.
 *
 * @param[in] arr The Array to enable Comparison-Mode on.
 * @param[in] comp_f The specialized Comparison-Function.
 * @return Returns 0 on Success or -1 on Error.
 * */
int array_setCompMode(array * arr, array_comp_f comp_f);

/**
 * @ingroup Array
 * @brief
 * Append any kind of Data to the Array. This Data has to be the
 * Size in Bytes, passed to the array_create Function otherwise,
 * behavior is unspecified.
 *
 * @param[in] arr The Array to append Data to.
 * @param[in] data The Pointer to the Data.
 * @return Returns the Index at which the Data was set when successful,
 * 				otherwise -1.
 * */
int array_append(array * arr, void * data);

/**
 * @ingroup Array
 * @brief
 * Use this function after enabling the sorting-mechanism with
 * {@link array_setCompMode}. All other functions are dissalowed, as they are
 * not adding the data in an orderly fashion.
 *
 * @param[in] arr The Array to which will be added.
 * @param[in] data a Pointer to data.
 * @return Returns the index at which the object was added or -1 on Error.
 * */
int array_addSorted(array * arr, void * data);

/**
 * @ingroup Array
 * @brief
 * Adds any kind of Data to the first fitting position. Will search the Array
 * for free space, or will append if there is no free space available.
 *
 * @note
 * This only makes real sense in REFERENCE MODE, because it checks for NULL.
 * If this is used with integers or structs that can be all Zero, then these
 * will be overwritten.
 *
 * @param[in] arr The Array to add Data to.
 * @param[in] data The Data to add.
 * @return Returns the Index at which the Data was placed or -1 on Error.
 * */
int array_add(array * arr, void * data);

/**
* @ingroup Array
* @brief
* Pops the end of the Array. This acts like {@link array_get getting} the last
* Element of the Array and then {@link array_remove removing} it.
*
* @note
* When you are in REFERENCE Mode, you have to pass a pointer to a pointer of
* the type you expect to retrieve.
* In VALUE Mode, you have to pass a pointer to the actual memory, to which
* the data will be written.
*
* @code
* // Reference Mode
* int * pValue = NULL;
* array_pop(arr, &pValue);
*
* // Value Mode
* int value;
* array_pop(arr, &value);
* @endcode
*
* @param[in] arr The Array to pop from.
* @param[out] data_out Where the Data will be copied to.
* @return Returns 0 on Success or -1 on Error.
*/
int array_pop(struct array * arr, void * data_out);

/**
 * @ingroup Array
 * @brief
 * Set any Index of the Array to the Data.
 *
 * NOTICE: If you set the Index out of bounds,
 * the array will be inflated so that the data will fit at the index.
 * All Data in between will not be nulled.
 *
 * @param[in] arr The Array.
 * @param[in] index The Index at which Data will be set. The Index may be larger
 * 			then the acutal length of the Array.
 * @param[in] data The Data to store.
 * @return Returns the Index at which the Data was set, otherwise -1.
 * */
int array_set(array * arr, int index, void * data);

/**
* @ingroup Array
* @brief
* Insert the Data at the the Index. All Data at and after the index are
* offsetted 1 Element. Please read the notice of array_set(). The same
* applies to this function.
*
* @param[in] arr The Array.
* @param[in] index The Index at which the data will be placed.
* @param[in] data The Data to store.
* @return Returns the Index at which the Data was placed or -1 on Error.
*/
int array_insertAt(array * arr, int index, void * data);

/**
 * @ingroup Array
 * @brief
 * Get the Data stored at any Index. In Reference-Mode, this will return the
 * exact Pointer which was fed to the Array, in non-Reference-Mode, this
 * will return a Pointer to the copied Data.
 *
 * @param[in] The Array.
 * @param[in] index The Index to get Data from.
 * @return A Pointer to the Data.
 * */
void * array_get(array * arr, int index);

/**
 * @ingroup Array
 * @brief
 * Delete the Data at any Index. This will free the
 * space taken in non-Reference-Mode or set the Value to NULL
 * in Reference-Mode.
 *
 * @param[in] arr The Array.
 * @param[in] index The Index to delete.
 * */
void array_delete(array * arr, int index);

/**
 * @ingroup Array
 * @brief
 * Delete the Data at any Index. Joins the
 * now free space together. So that there are no
 * holes in the Array.
 *
 * @param[in] arr The Array.
 * @param[in] index The Index to remove.
 * */
void array_remove(array * arr, int index);

/**
 * @ingroup Array
 * @brief
 * Delete the Object from the Array. This function will find the object
 * based on the pointer if no comparison function is set.
 *
 * @param[in] arr The Array.
 * @param[in] obj The Index to remove.
 * @see array_setCompMode
 * */
void array_remove_obj(array * arr, void * obj);

/**
 * @ingroup Array
 * @brief
 * Finds the given Data in the Array if present. The function will do optimized search,
 * if {@link array_setCompMode} had been enabled. If no comparison function was provided, the
 * function will do a regular search.
 *
 * @param arr The Array which will be searched.
 * @param data The Data that is to be found.
 * @return Returns the Index of the Data or -1 on Error.
 * */
int array_find(array * arr, void * data);


/**
 * @ingroup Array
 * @brief
 * Get the Length of the Array.
 *
 * @param[in] arr The Array.
 * @return The Length of the Array ot -1 if an error occurred.
 * */
int array_getLength(array * arr);

/**
 * @ingroup Array
 * @brief
 * Creates a copy of the Array. Will only copy what is directly stored. If the
 * stored objects themselves have Pointers to Data outside, this obiously will
 * not be duplicated. Only the Address of the Pointer will be copied. Which will
 * result in unexpected behavior and it is recommended only to copy arrays of
 * objects that do not have Pointers inside of them.
 *
 * @param[in] arr The Array which will be copied.
 * @return Returns the copy or NULL on Error.
 * */
array * array_copy(array * arr);

#ifdef __cplusplus
}
#endif

#endif
