#ifndef DS_ARRAY_HH_
#define DS_ARRAY_HH_

#include <iostream>

#include "array.h"
#include <memory>
#include <any>

namespace ds {

template<typename T>
class Array {
	private:
		std::shared_ptr<array> arr_s = nullptr;
		array * arr;
	public:

		Array() {
			this->arr_s = std::shared_ptr<array>((array*) array_create(sizeof(T)), array_free);
			this->arr = (array*) this->arr_s.get();
		}

		Array(Array& old) {
			this->arr_s = std::shared_ptr<array>((array*) array_copy(old.arr), array_free);
			this->arr = (array*) this->arr_s.get();
		}

		int set_comp_mode(array_comp_f comp_f) {
			return array_setCompMode(this->arr, comp_f);
		}

		int append(T data) {
			return array_append(this->arr, &data);
		}

		int add_sorted(T data) {
			return array_addSorted(this->arr, &data);
		}

		int add(T data) {
			return array_add(this->arr, &data);
		}

		int pop(T& data_out) {
			return array_pop(this->arr, &data_out);
		}

		int set(int index, T data) {
			return array_set(this->arr, index, &data);
		}

		int insert_at(int index, T data) {
			return array_insertAt(this->arr, index, &data);
		}

		T get(int index) {
			return *((T*)array_get(this->arr, index));
		}

		void delete_at(int index) {
			return array_delete(this->arr, index);
		}

		void remove(int index) {
			return array_remove(this->arr, index);
		}

		int find(T data) {
			return array_find(this->arr, &data);
		}

		int get_length() {
			return array_getLength(this->arr);
		}

		T& operator[](int index) {
			T* data = (T*)array_get(this->arr, index);
			return *data;
		}
};

}

#endif
