#ifndef BUFFER_H_
#define BUFFER_H_

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


/** @defgroup Buffer
 *
 * This is the Buffer Module.
 * A Buffer is a structure that allows for a controlled storage of bytes.
 * It grows automatically when the allocated memory for the buffer is not big
 * enough. The Buffer can work in 2 different modes.
 *
 */


/**
* @ingroup Buffer
* @brief
* Miscellaneous Buffer arguments and options.
*/
typedef enum {
	/** Create a Buffer in STACK Mode. More information on the Modes
	 * {@link buffer here}.*/
	BUFFER_MODE_STACK,

	/** Create a Buffer in TAPE Mode. More information on the Modes
	 * {@link buffer here}.*/
	BUFFER_MODE_TAPE,

	/** Clear a Buffer completely. */
	BUFFER_CLEAR_ALL,

	/** Clear only the already read part of the Buffer. */
	BUFFER_CLEAR_READ,

	/** Select the first occurrence. */
	BUFFER_FIRST,

	/** Select all occurrences. */
	BUFFER_ALL,

	/** Select the last occurrence. */
	BUFFER_LAST

} buffer_misc;

/**
* @ingroup Buffer
* @brief
* Decides on what character sequence a Line ends.
*/
typedef enum {
	/** The Line ends with \\r\\n */
	BUFFER_CRLF,

	/** The Line ends with \\n */
	BUFFER_LF,

	/** The Line ends with \\r */
	BUFFER_CR

} buffer_line_end;

/**
* @ingroup Buffer
* @brief
* A Buffer is a structure that allows for a controlled storage of bytes.
* It grows automatically when the allocated memory for the buffer is not big
* enough. The Buffer can work in 2 different modes.
*
* TAPE MODE:
*	The TAPE MODE allows the Buffer to be written to and read from more or
*	less seperatly. It acts like a Datastream. When Data is written to
*	the Buffer, this data becomes readable. When data is read from the
*	Buffer, the read data becomes unreadable (temporaily), as the Buffers
*	reading-head is incremented. It is easier to understand this when
*	imagining an acutal Tape, which is read while spooling over the Tape
*	for every read byte. To read data, which was already read, the TAPE
*	has to be rewound.
*
* STACK MODE:
*	The STACK MODE works like a Stack. You only have access to the Data
*	from the top. The Data read from a Stack-Buffer will be in the same
*	order as when it was written. So that a written string wont come out
*	reversed (unless you read fewer bytes than you have written).
*
*/
typedef struct buffer buffer;

/**
* @ingroup Buffer
* @brief
* Creates a new Buffer. The user has to free the allocated memory with
* {@link buffer_free buffer_free(buffer)}. A Buffer can have different modes,
* to get more information on that see {@link buffer Buffer}.
*
* @param[in] mode A mode like {@link BUFFER_MODE_TAPE} or {@link BUFFER_MODE_STACK}.
* @return Returns the {@link buffer Buffer} on Success or {@link NULL} on Error.
*/
buffer * buffer_create(int mode);

/**
* @ingroup Buffer
* @brief
* Free the allocated heap memory of the {@link buffer Buffer}.
* @param[in] b The Buffer, which will be unreferencable afterwards.
*/
void buffer_free(buffer * b);

/**
* @ingroup Buffer
* @brief
* Clears the Buffer and resets/rewinds it based on the given option.
* To clear it completely, use {@link BUFFER_CLEAR_ALL}.
* To clear only read parts, use {@link BUFFER_CLEAR_READ}
*
* @param[in] b The Buffer to clear.
*/
void buffer_clear(buffer * b, int option);

/**
* @ingroup Buffer
* @brief
* This does nothing, when in STACK mode.
* In TAPE mode, the tape-pointer will be moved n Bytes back to the front.
* Use n=0 to reset the tape-pointer to the beginning of the tape. Rewinding
* allows the user to read all written data from the beginning.
*
* @param[in] b The Buffer to rewind.
* @param[in] n Amount of bytes to rewind (n=0 => set to beginning).
*/
void buffer_rewind(buffer * b, int n);

/**
* @ingroup Buffer
* @brief
* Get the amount of currently readable bytes.
*
* @param[in] b The Buffer to get the length from.
* @return Returns the amount of currently readable bytes.
*/
int buffer_getLength(buffer * b);

/**
* @ingroup Buffer
* @brief
* Get the Position at which the next Byte will be read.
* The Position reflects the amount of "skipped" Bytes.
*
* @param[in] b The Buffer from which the Position will be read.
* @return Returns The Position of -1 on Error.
*/
int buffer_getPosition(buffer * b);

/**
* @ingroup Buffer
* @brief
* Replaces the specified Byte with another Byte. Use the options to specify
* a replacement rule. Supported selectors are: {@link BUFFER_FIRST},
* {@link BUFFER_LAST}, and {@link BUFFER_ALL}.
*
* @param[in] b The Buffer to replace something in.
* @param[in] old The value of the Byte which will be replaced.
* @param[in] new The value of the Byte which will be inserted.
* @param[in] option A selector from the selectors listed above.
* @return Returns the amount of replaced Bytes or -1 on Error.
* @see buffer_misc
*/
int buffer_replace(buffer * b, uint8_t old, uint8_t new_, int option);

/**
* @ingroup Buffer
* @brief
* Skips leading Whitespace. The Whitespace will not be removed, but the Buffer's
* position will adjust accordingly.
*
* @param b The Buffer to modify.
* @return Returns the amount of Whitespace skipped or -1 on Error.
*/
int buffer_skipWhite(buffer * b);

/**
* @ingroup Buffer
* @brief
* Skip the specified amount of Bytes.
*
* @param[in] b The Buffer to modify.
* @param[in] amount The amount of Bytes to skip.
* @return Returns the amount of skipped Bytes or -1 on Error.
 * */
int buffer_skip(buffer * b, int amount);

/**
* @ingroup Buffer
* @brief
* Writes n Bytes from a Byte-Array to the Buffer.
*
* @param[in] b The buffer to write to.
* @param[in] bbuf A byte-buffer from which will be read.
* @param[in] n The amount of bytes to copy.
* @return Returns the amount of actually written bytes on Success or -1 on Error.
*/
int buffer_write(buffer * b, uint8_t * bbuf, size_t n);

/**
* @ingroup Buffer
* @brief
* Writes an Integer i (32-Bit) to the buffer b.
* The Function will not convert between little/big endianess.
* In case a specific byte-order is nessecary, the user has to
* convert the original Integer.
*
* @param[in] b The buffer to write to.
* @param[in] i An 32-Bit Integer.
* @return Returns 0 on Success or -1 on Error.
*/
int buffer_writeInt(buffer * b, uint32_t i);

/**
* @ingroup Buffer
* @brief
* Writes an Integer l (64-Bit) to the buffer b.
* The Function will not convert between little/big endianess.
* In case a specific byte-order is nessecary, the user has to
* convert the original Integer.
*
* @param[in] b The buffer to write to.
* @param[in] l An 64-Bit Integer.
* @return Returns 0 on Success or -1 on Error.
*/
int buffer_writeLong(buffer * b, uint64_t l);

/**
* @ingroup Buffer
* @brief
* Writes an floating-point Number (32-Bit) to the buffer b.
* The Function will not convert between little/big endianess.
* In case a specific byte-order is nessecary, the user has to
* convert the original Float.
*
* @param[in] b The buffer to write to.
* @param[in] l An 32-Bit fp Number.
* @return Returns 0 on Success or -1 on Error.
*/
int buffer_writeFloat(buffer * b, float f);

/**
* @ingroup Buffer
* @brief
* Writes an floating-point Number (64-Bit) to the buffer b.
* The Function will not convert between little/big endianess.
* In case a specific byte-order is nessecary, the user has to
* convert the original Float.
*
* @param[in] b The buffer to write to.
* @param[in] l An 64-Bit fp Number.
* @return Returns 0 on Success or -1 on Error.
*/
int buffer_writeDouble(buffer * b, double d);

/**
* @ingroup Buffer
* @brief
* Read n Bytes from the Buffer. Will only read, when a greater or equal amount
* of Bytes are buffered. Otherwise the function will return -1.
*
* @param[in] b The buffer to read from.
* @param[out] bbuf The byte-buffer to write to.
* @param[in] n Maximum amount of bytes to read.
* @return Returns the amount of read bytes on Success or -1 on Error.
*/
int buffer_read(buffer * b, uint8_t * bbuf, size_t n);

/**
* @ingroup Buffer
* @brief
* Reads from the buffer line-wise. This only works on MODE_TAPE. The string will
* not be modified, which means, the closing '\0'-Byte won't be appended.
* The user has to do this for themself. The Return Value indicates the length
* of the read line.
*
* @param[in] b The buffer to read from.
* @param[out] bbuf The byte-buffer to write to.
* @param[in] n Maximum amount of bytes to read.
* @param[in] line_end The type of line-end.
* @return Returns the amount of read bytes or -1 on Error.
*/
int buffer_readLine(buffer * b, uint8_t * bbuf, size_t n, buffer_line_end line_end);

/**
* @ingroup Buffer
* @brief
* Reads from the Buffer a Token. A Token is defined as a string of characters
* without spaces. So this function will read until the next space or until.
* the Buffer is empty. The function will not append the '\0'-Byte at the end.
* Will skip any whitespace, even if no Token could be read.
*
* This works only in MODE_TAPE.
*
* @param[in] b The Buffer to read from.
* @param[out] bbuf The byte-buffer to write to.
* @param[in] n The maximum amount of bytes to read.
* @return Returns the amount of Bytes read (without leading whitespace)
* 	or -1 on Error.
*/
int buffer_readToken(buffer * b, uint8_t * bbuf, size_t n);

/**
* @ingroup Buffer
* @brief
* Reads an 32-Bit Integer from the buffer b.
* The result will be written to i.
*
* @param[in] b The buffer to read from.
* @param[out] i Where the result will be saved.
* @return Returns 0 on Success or -1 on Error.
*/
int buffer_readInt(buffer * b, uint32_t * i);

/**
* @ingroup Buffer
* @brief
* Reads an 64-Bit Integer from the buffer b.
* The result will be written to l.
*
* @param[in] b The buffer to read from.
* @param[out] l Where the result will be saved.
* @return Returns 0 on Success or -1 on Error.
*/
int buffer_readLong(buffer * b, uint64_t * l);

/**
* @ingroup Buffer
* @brief
* Reads an 32-Bit fp Number from the buffer b.
* The result will be written to f.
*
* @param[in] b The buffer to read from.
* @param[out] f Where the result will be saved.
* @return Returns 0 on Success or -1 on Error.
*/
int buffer_readFloat(buffer * b, float * f);

/**
* @ingroup Buffer
* @brief
* Reads an 64-Bit fp Number from the buffer b.
* The result will be written to d.
*
* @param[in] b The buffer to read from.
* @param[out] d Where the result will be saved.
* @return Returns 0 on Success or -1 on Error.
*/
int buffer_readDouble(buffer * b, double * d);

/**
 * @ingroup Buffer
 * @brief
 * Returns the underlying Byte-Buffer directly.
 *
 * @param[in] b The Buffer to get the Byte-Buffer from.
 * @return Returns the underlying Byte-Buffer or NULL on Error.
 * */
uint8_t * buffer_getBuffer(buffer * b);

#ifdef __cplusplus
}
#endif


#endif
