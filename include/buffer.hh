#include "buffer.h"

#include <memory>

namespace ds {

class Buffer {
	public:
		enum class Mode {
			TAPE = BUFFER_MODE_TAPE,
			STACK = BUFFER_MODE_STACK
		};

		enum class LineEnd {
			LF = BUFFER_LF,
			CR = BUFFER_CR,
			CRLF = BUFFER_CRLF
		};

		enum class Clear {
			ALL = BUFFER_CLEAR_ALL,
			READ = BUFFER_CLEAR_READ
		};

		enum class Select {
			ALL = BUFFER_ALL,
			FIRST = BUFFER_FIRST,
			LAST = BUFFER_LAST
		};

	private:
		std::shared_ptr<buffer> buf_s = nullptr;
		buffer * buf;

	public:
		Buffer(Mode mode) {
			this->buf_s = std::shared_ptr<buffer>((buffer*)buffer_create((int)mode), buffer_free);
			this->buf = this->buf_s.get();
		}

		void clear(Clear option) {
			buffer_clear(this->buf, (int)option);
		}

		void rewind(int n) {
			buffer_rewind(this->buf, n);
		}

		int get_length() {
			return buffer_getLength(this->buf);
		}

		int get_position() {
			return buffer_getPosition(this->buf);
		}

		int replace(uint8_t old, uint8_t new_, Select option) {
			return buffer_replace(this->buf, old, new_, (int)option);
		}

		int skip_white() {
			return buffer_skipWhite(this->buf);
		}

		int skip(int amount) {
			return buffer_skip(this->buf, amount);
		}

		int write(uint8_t * bbuf, size_t n) {
			return buffer_write(this->buf, bbuf, n);
		}

		int write_int(uint32_t i) {
			return buffer_writeInt(this->buf, i);
		}

		int write_long(uint64_t l) {
			return buffer_writeLong(this->buf, l);
		}

		int write_float(float f) {
			return buffer_writeFloat(this->buf, f);
		}

		int write_double(double d) {
			return buffer_writeDouble(this->buf, d);
		}

		int read(uint8_t * bbuf, size_t n) {
			return buffer_read(this->buf, bbuf, n);
		}

		int read_line(uint8_t * bbuf, size_t n, LineEnd line_end) {
			return buffer_readLine(this->buf, bbuf, n, (buffer_line_end)line_end);
		}

		int read_token(uint8_t * bbuf, size_t n) {
			return buffer_readToken(this->buf, bbuf, n);
		}

		int read_int(uint32_t& i) {
			return buffer_readInt(this->buf, &i);
		}

		int read_long(uint64_t& l) {
			return buffer_readLong(this->buf, &l);
		}

		int read_float(float& f) {
			return buffer_readFloat(this->buf, &f);
		}

		int read_double(double& d) {
			return buffer_readDouble(this->buf, &d);
		}

		uint8_t * get_buffer() {
			return buffer_getBuffer(this->buf);
		}
};

}