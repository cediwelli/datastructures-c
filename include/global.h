#ifndef DS_GLOBAL_H_
#define DS_GLOBAL_H_

#define DS_ASSERT_NON_NULL(obj, e) if ((obj) == NULL) {e;}
#define DS_ASSERT_EQL_NULL(obj, e) if ((obj) == NULL) {e;}
#define DS_ASSERT_EQL(obj1, obj2, e) if ((obj1) != (obj2)) {e;}
#define DS_ASSERT_NOT_EQL(obj1, obj2, e) if ((obj1) == (obj2)) {e;}
#define DS_ASSERT_GEQ(obj1, obj2, e) if ((obj1) < (obj2)) {e;}

#endif
