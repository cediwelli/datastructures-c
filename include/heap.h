#ifndef HEAP_H_
#define HEAP_H_

#include "array.h"
#include <stddef.h>

/**
 * @defgroup Heap
 * A Heap is a structure that allows for constant-time access to the either
 * heighest priority or lowest priority Element in the Set to be accessed.
 * To access the lowest priority Elements, the MIN-Heap is used, for access to
 * the highest priority Element, the MAX-Heap is used.
 * */

/**
 * @ingroup Heap
 * @brief
 * Used to declare the Heap as a MIN-Heap.
 * @see heap_create
 * */
#define HEAP_MIN 0

/**
 * @ingroup Heap
 * @brief
 * Used to declare the Heap as a MAX-Heap.
 * @see heap_create
 * */
#define HEAP_MAX 1

/**
 * @ingroup Heap
 * @brief
 * A Heap is a structure that allows for constant-time access to the either
 * heighest priority or lowest priority Element in the Set to be accessed.
 * To access the lowest priority Elements, the MIN-Heap is used, for access to
 * the highest priority Element, the MAX-Heap is used.
 * */
typedef struct heap heap;
typedef struct heap_element heap_element;

/**
 * @ingroup Heap
 * @brief
 * Creates a new MIN or MAX Heap. The Heap can store its Elements either as
 * REFERENCE or VALUE. To store it as a REFERENCE, use 0 as the size, to store
 * it as VALUE, which means, that the value of the passed Element will be
 * copied, pass the actual size of the Datatype in Bytes. The Heap has to be
 * free'd with {@link heap_free} by the user, when the Heap is no longer needed.
 *
 * @param[in] size The size of the Data that is going to be inserted into the
 * 				Heap. Use 0 for by-reference mode.
 * @param[in] type The Type of the Heap (MIN/MAX). You can use HEAP_MIN or
 * 				HEAP_MAX.
 * @return Returns the Heap-Object-Pointer or NULL on Error.
 * */
heap * heap_create(size_t size, int type);

/**
 * @ingroup Heap
 * @brief
 * Free the allocated memory of the Heap.
 *
 * @param[in] h The Heap of which the memory will be free'd.
 * */
void heap_free(heap * h);

/**
 * @ingroup Heap
 * @brief
 * Add any Element to the Heap and set its priority.
 * The priority will determine the position in the Heap.
 *
 * @param[in] h The Heap to which data will be added.
 * @param[in] data A Pointer to any data.
 * @param[in] priority The Position in the Heap.
 *
 * @return Returns 0 on success or -1 on Error.
 * */
int heap_add(heap * h, void * data, int priority);

/**
 * @ingroup Heap
 * @brief
 * Returns the top-most Element from the Heap.
 *
 * When initilized as REFERENCE, pass a empty void **. Otherwise using the VALUE
 * mode, pass a void * to where the data will be copied.
 *
 * @param[in] h The Heap to pop from.
 * @param[out] data_out A Pointer to either a void** in by-reference mode or a
 * 				void* to allocated memory in by-value mode.
 *
 * @return Returns 0 on success or -1 on Error.
 * @see heap_peek
 * */
int heap_pop(heap * h, void * data_out);

/**
 * @ingroup Heap
 * @brief
 * The same as {@link heap_pop} but without removing the Element.
 * @param[in] h The Heap to pop from.
 * @param[out] data_out A Pointer to either a void** in by-reference mode or a
 * 				void* to allocated memory in by-value mode.
 * @return Returns 0 on success or -1 on Error.
 * */
int heap_peek(heap * h, void * data_out);

#endif
