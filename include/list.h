#ifndef LIST_H_
#define LIST_H_

#include <stddef.h>

/**
 * @defgroup List
 * @brief
 * The List is the implementation of a linked List. It manages its memory
 * automatically and typical linked List operations are possible. The List
 * also icorporates a linked Queue as operations like prepending are available.
 * */

/**
 * @ingroup List
 * @brief
 * The List is the implementation of a linked List. It manages its memory
 * automatically and typical linked List operations are possible. The List
 * also icorporates a linked Queue as operations like prepending are available.
 * */
typedef struct list list;

/**
 * @ingroup List
 * @brief
 * A {@link list_element} defines the recursive structure that makes up the
 * links between the Elements in the List. This structure should be treated as
 * read-only! It is necessary for the user to be able to read from this
 * structure because of the {@link list_iterator}, which makes iterating over
 * a List more performant.
 * */
typedef struct list_element list_element;

/**
 * @ingroup List
 * @brief
 * @see list_element
 * */
typedef list_element list_iterator;

/**
 * @ingroup List
 * */
struct list_element {
	void * value;
	list_element * next;
	list_element * prev;
};

/**
 * @ingroup List
 * @brief
 * Create a new List. The User has to set the Size in Bytes
 * one piece of Data takes. Pass 0 to enter Reference-Mode,
 * which will not copy Data but just take the Data-Pointers
 * as-is.
 *
 * @param[in] size The Size of the Data in Bytes. Use 0 to indicate Referencing.
 * @return Returns a Pointer to the List or -1 if an Error occurred.
 * */
list * list_create(size_t size);

/**
 * @ingroup List
 * @brief
 * To free the space a List takes.
 * @param[in] l The List.
 * */
void list_free(list * l);

/**
 * @ingroup List
 * @brief
 * Checks if the passed list is empty or not.
 *
 * @param[in] l The List to check.
 * @return Returns TRUE(1) if List is empty or FALSE(0) if not.
 * */
int list_isEmpty(list * l);

/**
 * @ingroup List
 * @brief
 * Push Data onto the List. The Data will be appended to End of the List.
 * @param[in] l The List.
 * @param[in] v A Pointer to the Data.
 * @see list_prepend
 * */
void list_append(list * l, void * v);

/**
 * @ingroup List
 * @brief
 * Prepend Data before the List. The Data will be at the very Front.
 * @param[in] l The List.
 * @param[in] v A Pointer to the Data.
 * @see list_append
 * */
void list_prepend(list * l, void * v);

/**
 * @ingroup List
 * @brief
 * Insert Data into the List. The Data will be inserted at
 * the passed Index.
 *
 * @param[in] l The List.
 * @param[in] index The Index to insert the Data into.
 * @param[in] v A Pointer to the Data.
 * */
void list_insert(list * l, int index, void * v);

/**
 * @ingroup List
 * @brief
 * Pops the end of the List. The List's length will decrease after using this
 * Function. The Data, which is to be popped, will be written into out.
 * The pointer that needs to be passed depends on the type of List.
 *
 * When the List was initilized as REFERENCED, then the passed
 * void* has to be a pointer to a pointer of the Data.
 *
 * When the List was initilized as VALUE, then the passed void*
 * has to be a pointer to an actual chunk of memory to which the entirety of the
 * data will be written.
 *
 * Example:
 * @code
 * // value mode:
 *
 * int value;
 * int ret = list_pop(l, &value);
 *
 * printf("My Value is: %d\n", value);
 *
 * // referenced mode:
 *
 * int * ref_value = NULL;
 * int ret = list_pop(l, &ref_value);
 *
 * printf("My Value is: %d\n", *ref_value);
 * @endcode
 *
 * @param[in] l The List to pop Data from.
 * @param[out] out A pointer as described above.
 * @return Returns TRUE (1) if there is still Data in the List available or
 * 			FALSE (0) if there is not Data to be popped.
 * @see list_popFront
 * */
int list_popBack(list * l, void * out);

/**
 * @ingroup List
 * @brief
 * Pops the Front of the List. To get more information on how this works
 * exactly, see {@link list_popBack}
 *
 * @param[in] l The List to pop Data from.
 * @param[out] out A pointer as described above.
 * @return Returns TRUE (1) if there is still Data in the List available or
 * 			FALSE (0) if there is not Data to be popped.
 * @see list_popBack
 * */
int list_popFront(list * l, void * out);

/**
 * @ingroup List
 * @brief
 * Get the Data at the specified Index. The returned pointer should not be
 * free'd when VALUE was selected, as the pointer points to the
 * copy that was made by adding it.
 * If REFERENCE was selected, the returned pointer is exactly
 * the same as the inserted pointer.
 * For iteration, use the {@link list_iterator}. It will be much faster.
 *
 * @param[in] l The List.
 * @param[in] index The Index to get the Data from.
 * @return Returns a Pointer to the Data or -1 on Error.
 * */
void * list_get(list * l, int index);

/**
 * @ingroup List
 * @brief
 * Remove Data from the List at the given Index.
 *
 * @param[in] l The List.
 * @param[in] index The Index of the Data that should be removed.
 * */
void list_remove(list * l, int index);

/**
 * @ingroup List
 * @brief
 * Will get the Iterator of the List. The Iterator is the more efficient way
 * to iterate over the List, as {@link list_get} has to iterate over most of the
 * List itself to even get to the specified Index.
 * \n\n
 * Example:
 * @code
 * for(list_iterator * it = list_getIterator(list); it; it = it->next) {
 *     ...
 * }
 * @endcode
 *
 * @param[in] l The List.
 * @return Returns the Iterator or -1 on Error.
 * */
list_iterator * list_getIterator(list * l);

#endif
