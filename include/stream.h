#ifndef DS_STREAM22_H_
#define DS_STREAM22_H_

#include <stdint.h>

/** @defgroup Stream
 *
 * A Stream acts like a customizable pipe. From the IN-End, data will be read and
 * then written to the OUT-End. The Stream's purpose is to provide a simple way
 * to transfer data from a stream-like file-descriptor to any of the other
 * datastructures in the library. The Stream however is not limited to this
 * specific use-case. With the ability to write custom IN and OUT functions,
 * basically every operation is possible.
 *
 * <b>Multicast functionality</b>\n
 * With the addition of {@link stream_addIn} and {@link stream_addOut},
 * multicasting now becomes a possibility with m:n multiplexing.
 * In most cases, a 1:n or n:1 MUX/DEMUX will be sufficient but the Stream is
 * not limited to this and m:n are very much possible.
 *
 * \n\n
 * Feel free to use the predefined functions for transmissions:
 * {@link stream_fromFD}, {@link stream_toFD}, {@link stream_fromBuffer},
 * {@link stream_toBuffer}
 * @see Stream_Predefined
 */

/**
* @defgroup Stream_Predefined
* \ingroup Stream
*/

/**
* @ingroup Stream
* @brief
* A Stream acts like a customizable pipe. From the IN-End, data will be read and
* then written to the OUT-End. The Stream's purpose is to provide a simple way
* to transfer data from a stream-like file-descriptor to any of the other
* datastructures in the library. The Stream however is not limited to this
* specific use-case. With the ability to write custom IN and OUT functions,
* basically every operation is possible.
*
*/
typedef struct stream stream;

/**
* @ingroup Stream
* @brief
* Definition of how a IN/OUT function must look like.
* The first parameter fd can be anything the custom IN/OUT function likes.
* In the case of a Socket of Pipe this would be a file-descriptor. In case of
* a {@link buffer Buffer}, this could be a {@link buffer buffer*}.
*
* @param[in] fd A file-descriptor, pointer or whatever the custom function needs.
* @param[in,out] buf A character Buffer from which is read or to which will be
*			written.
* @param[in] n The amount of Bytes or Objects to read/write.
* @return Returns the number of Bytes/Objects written or read or -1 on Error.
*/
typedef uint32_t (*stream_func)(uint64_t fd, uint8_t * buf, uint32_t n);

/**
* @ingroup Stream
* @brief
* A Datastructure that represents one Endpoint of a Stream. The Endpoint does
* consist from the descriptor and the function. The function takes the
* descriptor and executes its operations on it.
 * */
typedef struct {
	uint64_t    stream_fd;
	stream_func stream_f;
} endpoint_t;

/**
* @ingroup Stream_Predefined
* @brief
* Predefined Function for reading from a file-descriptor. The User may use this
* Function as an IN Function when the User wishes to read from a file-descriptor.
* @see stream_func
*/
extern uint32_t stream_fromFD(uint64_t fd, uint8_t * buf, uint32_t n);

/**
* @ingroup Stream_Predefined
* @brief
* Predefined Function for writing to a file-descriptor. The User may use this
* Function as an OUT Function when the User wishes to write to a file-descriptor.
* @see stream_func
*/
extern uint32_t stream_toFD(uint64_t fd, uint8_t * buf, uint32_t n);

/**
* @ingroup Stream_Predefined
* @brief
* Predefined Function for reading from a {@link buffer Buffer}. The User may use
* this Function as an IN Function when the User wishes to read from a
* {@link buffer Buffer}.
* @see stream_func
*/
extern uint32_t stream_fromBuffer(uint64_t fd, uint8_t * buf, uint32_t n);

/**
* @ingroup Stream_Predefined
* @brief
* Predefined Function for writing to a {@link buffer Buffer}. The User may use
* this Function as an OUT Function when the User wishes to write to a
* {@link buffer Buffer}.
* @see stream_func
*/
extern uint32_t stream_toBuffer(uint64_t fd, uint8_t * buf, uint32_t n);

/**
* @ingroup Stream
* @brief
* Creates a new {@link stream Stream}. The User has to free the allocated memory
* with {@link stream_free stream_free} after being done with the
* {@link stream Stream}. Before it can be used, the IN/OUT Functions have to be
* assigned via {@link stream_setIn} and {@link stream_setOut}.
*
* @param[in] transmit_amount How many bytes should be transmitted at max.
* @return Returns the {@link stream Stream}.
*/
stream * stream_create(uint32_t transmit_amount);

/**
* @ingroup Stream
* @brief
* Frees the allocated memory of the {@link stream Stream}.
* Must be called after the User is done with the {@link stream Stream}.
*
* @param[in] stream The {@link stream Stream} to free.
*/
void stream_free(stream * stream);

/**
* @deprecated Use {@link stream_addIn}
* @ingroup Stream
* @brief
* Set the Function for the INput. From here, the {@link stream Stream} will
* read the Data.
*
* @param[in] stream The {@link stream Stream} which will be modified.
* @param[in] in_fd The file-descriptor or pointer for the IN-Function.
* @param[in] f The IN-Function, that will be attached to the {@link stream Stream}.
*/
void stream_setIn(stream * stream, uint64_t in_fd, stream_func f);

/**
* @deprecated Use {@link stream_addOut}
* @ingroup Stream
* @brief
* Set the Function for the OUTput. To this, the {@link stream Stream} will
* write the Data.
*
* @param[in] stream The {@link stream Stream} which will be modified.
* @param[in] out_fd The file-descriptor or pointer for the OUT-Function.
* @param[in] f The OUT-Function, that will be attached to the {@link stream Stream}.
*/
void stream_setOut(stream * stream, uint64_t out_fd, stream_func f);

/**
* @ingroup Stream
* @brief
* Adds a Function for the INput. To this, the {@link stream Stream} will
* write the Data.
*
* @param[in] stream The {@link stream Stream} which will be modified.
* @param[in] in_fd The file-descriptor or pointer for the IN-Function.
* @param[in] f The IN-Function, that will be attached to the {@link stream Stream}.
* @return Returns the Index at which the Function was added. This can be used to
* 			remove the Function from the {@link stream Stream}.
*/
int stream_addIn(stream * stream, uint64_t in_fd, stream_func f);

/**
* @ingroup Stream
* @brief
* Removes a Function from the INput. Use the Index received by
* {@link stream_addIn} to remove a Function.
*
* @param[in] stream The {@link stream Stream} which will be modified.
* @param[in] idx Index received by {@link stream_addIn}.
*/
void stream_remIn(stream * stream, int idx);

/**
* @ingroup Stream
* @brief
* Adds a Function for the OUTput. To this, the {@link stream Stream} will
* write the Data.
*
* @param[in] stream The {@link stream Stream} which will be modified.
* @param[in] out_fd The file-descriptor or pointer for the OUT-Function.
* @param[in] f The OUT-Function, that will be attached to the {@link stream Stream}.
* @return Returns the Index at which the Function was added. This can be used to
* 			remove the Function from the {@link stream Stream}.
*/
int stream_addOut(stream * stream, uint64_t out_fd, stream_func f);

/**
* @ingroup Stream
* @brief
* Removes a Function from the OUTput. Use the Index received by
* {@link stream_addOut} to remove a Function.
*
* @param[in] stream The {@link stream Stream} which will be modified.
* @param[in] idx Index received by {@link stream_addOut}.
*/
void stream_remOut(stream * stream, int idx);

/**
* @ingroup Stream
* @brief
* This will transmit try to transmit the set amount of Bytes/Objects from the
* IN-End to the OUT-End. It may happen, that the actually transmitted Objects
* differ from the set amount. The amount of read Bytes are written to the
* amounts array.
*
* @param[in] stream The Stream on which the transaction will be done.
* @param[out] amounts An Array of amounts with n Elements where n equals
* 	count of OUT-Endpoints or NULL.
* @return Returns 0 on Success or -1 on Error.
*/
uint32_t stream_transmit(stream * stream, uint32_t * amounts);

/**
* @ingroup Stream
* @brief
* Pumps Data from the IN-Endpoint to the OUT-Endpoint. The preferred amount of
* transmitted Data is set in n. The Function will return the actual amount of
* read/written Data.
*
* @param[in] in A Pointer to a custom IN-Endpoint.
* @param[in] out A Pointer to a custom OUT-Endpoint.
* @param[in] n The Amout of Data that is at max to be transmitted.
* @return Returns the amount of read/written Data or -1 on Error.
 * */
uint32_t stream_pump(endpoint_t * in, endpoint_t * out, int n);

#endif
