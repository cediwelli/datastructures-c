#ifndef DS_STRING_H_
#define DS_STRING_H_

/** @defgroup Strn
 *
 * The Strn Module enables dynamically sized strings at runtime.
 *
 * @see strn
 */

/**
 * @ingroup Strn
 * @brief
 * The Strn automatically allocates the necessary memory for a string.
 * */
typedef struct strn strn;

/**
 * @ingroup Strn
 * @brief
 * Creates a new dynamic string. A Strn has to be destroyed with {@link strn_free}.
 * @return Returns the Strn-Object.
 * */
strn * strn_create();

/**
 * @ingroup Strn
 * @brief
 * Destroys the Strn and clears its memory. Use this to prevent memory leaks.
 * @parma s the Strn-Object to destroy.
 * */
void strn_free(strn * s);

/**
 * @ingroup Strn
 * @brief
 * Set the Strn to a specific string-value.
 * @param s The Strn-Object which will be modified.
 * @param str The new string-value of the Strn.
 * @return Returns 0 on Success or -1 on Error.
 * */
int strn_set(strn * s, char * str);

/**
 * @ingroup Strn
 * @brief
 * Append a string to the Strn.
 * @param s The Strn-Object which will be modified.
 * @param str The string-value that will be appended to the Strn.
 * @return Returns 0 on Success or -1 on Error.
 * */
int strn_append(strn * s, char * str);

/**
 * @ingroup Strn
 * @brief
 * Get the actual byte-representation of the Strn. Because of dynamic resizing, it is not
 * guaranteed that the resulting pointer will stay the same! It is strongly advised to
 * get the pointer to the string everytime - do not cache the pointer in a variable.
 * @param s The Strn-Object from which will be read.
 * @return Returns the pointer to the byte array on Success or NULL on Error.
 * */
char * strn_getString(strn * s);

#endif