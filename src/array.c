#include "array.h"
#include "global.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IDX(idx) ((idx)*(arr->data_size))
#define DATA_ARR_MAX 32

struct array {
	/** The amount of Elements in the Array */
	unsigned int length;

	/** How much space in the Array there actually is */
	unsigned int capacity;
	unsigned int base_capacity;

	/** Either 0 (=> reference mode) or the size of the Elements in Bytes.
	 *  This is used for malloc-ing the correct size for the Elements.
	 * */
	unsigned int data_size;

	/** Is set when data_size==0 */
	unsigned int by_reference;

	/** Where all the pointers are stored */

	char * data_bands[DATA_ARR_MAX];
	int data_band_ptr;

	array_comp_f comp_f;
};

static unsigned long getSizeOfBand(int bandPtr, int baseCapacity) {

	unsigned long pow2 = 1;
	for (int i = 0; i < bandPtr; ++i) {
		pow2 *= 2;
	}
	return pow2 * baseCapacity;
}

static unsigned long getSizeOfBandsLower(int bandPtr, int baseCapacity) {

	unsigned long acc = 0;

	for (int i = 0; i < bandPtr; ++i) {
		acc += getSizeOfBand(i, baseCapacity);
	}
	return acc;
}

static int getBandByIndex(int index, int baseCapacity) {

	unsigned long pow2 = baseCapacity;
	unsigned long prev = 0;

	for (int i = 0; i < DATA_ARR_MAX; ++i) {
		if (index < pow2+prev) {
			return i;
		}
		prev += pow2;
		pow2 *= 2;
	}
	return -1;
}

static void * getPtr(int index, array * arr) {

	unsigned long band   = getBandByIndex(index, arr->base_capacity);
	unsigned long offset = 0;

	if (band > arr->data_band_ptr) {
		return NULL;
	}

	offset = index - getSizeOfBandsLower(band, arr->base_capacity);

	char * ptr = &(arr->data_bands[band][offset*(arr->data_size + 1)]);

	// return off by one because of additional status byte
	return ptr + 1;
}

static char * getStatusPtr(int index, array * arr) {
	unsigned long band   = getBandByIndex(index, arr->base_capacity);
	unsigned long offset = 0;

	offset = index - getSizeOfBandsLower(band, arr->base_capacity);

	char * ptr = &(arr->data_bands[band][offset*(arr->data_size + 1)]);
	return ptr;
}


static void set(int index, array * arr, char set_unset) {
	char * ptr = getPtr(index, arr);
	char * ptrStatus = ptr - 1;
	*ptrStatus = set_unset;
}

static int isSet(int index, array * arr) {
	void * ptr = getPtr(index, arr);
	return *(((char *)ptr) - 1) == 1;
}

static int binarySearchIndex(array * arr, void * new_data) {
	int high   = array_getLength(arr)-1;
	int low    = 0;
	int median = (high+low)/2;
	int compVal;

	void * highObj = array_get(arr, high);
	void * lowObj  = array_get(arr, low);

	if (arr->comp_f(new_data, highObj) == 1) {
		return high+1;
	}
	if (arr->comp_f(new_data, lowObj) <= 0) {
		return low;
	}

	while ( 1 ) {
		void * medianObj = array_get(arr, median);
		compVal   = arr->comp_f(new_data, medianObj);

		switch (compVal) {
			case 1:
				low = median;
				break;
			case 0:
				return median;
			case -1:
				high = median;
				break;
			case -2:
				return -1;
		}

		median = (high + low)/2;

		if (high - low <= 1) {
			if (compVal <= 0) {
				lowObj  = array_get(arr, low);
				compVal = arr->comp_f(new_data, lowObj);

				if (compVal <= 0) {
					return low;
				} else {
					return low+1;
				}
			} else {
				return median+1;
			}
		}
	}
	fprintf(stderr, "Placed at %d\n", median);
	return median;
}

static int doCompare(array * arr, void * new_data) {
	if (arr->comp_f != NULL && new_data) {
		int indexToInsertAt;

		if (arr->length == 0) {
			return array_set(arr, 0, new_data);
		} else if (arr->length == 1) {
			int compVal = 0;
			compVal = arr->comp_f(new_data, array_get(arr, 0));
			if (compVal <= -1) {
				array_set(arr, 1, array_get(arr, 0));
				return array_set(arr, 0, new_data);
			} else if (compVal >= 0) {
				return array_set(arr, 1, new_data);
			}
		}

		indexToInsertAt = binarySearchIndex(arr, new_data);
		if (indexToInsertAt == -1) {
			return -1;
		}

		return array_insertAt(arr, indexToInsertAt, new_data);
	}
	return -1;
}

/**
 * @brief
 * Inflates the Array, which means, that the memory is expanded so that more
 * Elements will fit into the Array. This grows exponentially.
 *
 * @param arr The Array to inflate.
 * */
static void inflate(struct array * arr) {

	int newBandSize = 0;

	arr->data_band_ptr += 1;
	newBandSize         = getSizeOfBand(arr->data_band_ptr, arr->base_capacity);
	arr->capacity      += newBandSize;

	// allocate and set one additional status byte
	arr->data_bands[arr->data_band_ptr] = malloc(newBandSize*(arr->data_size + 1));
	memset(arr->data_bands[arr->data_band_ptr], 0, newBandSize*(arr->data_size + 1));
}

struct array * array_create(size_t data_size) {

	struct array * arr = (struct array *) malloc(sizeof(struct array));

	arr->data_size = (data_size==0)*sizeof(void *) + data_size;

	arr->length        = 0;
	arr->base_capacity = 4;
	arr->capacity      = arr->base_capacity;

	// allocate one status additional byte
	arr->data_bands[0] = malloc((1 + arr->data_size)*arr->capacity);
	arr->data_band_ptr = 0;

	arr->by_reference = data_size==0;
	arr->comp_f       = NULL;

	memset(arr->data_bands[0], 0, (arr->data_size + 1)*arr->capacity);
	return arr;
}

void array_free(struct array * arr) {

	DS_ASSERT_NON_NULL(arr, return);

	for (int i = 0; i <= arr->data_band_ptr; ++i) {
		free(arr->data_bands[i]);
	}
	free(arr);
}

int array_setCompMode(array * arr, array_comp_f comp_f) {

	DS_ASSERT_NON_NULL(arr, return -1);
	DS_ASSERT_NON_NULL(comp_f, return -1);

	arr->comp_f = comp_f;
	return 0;
}

int array_append(array * arr, void * data) {

	DS_ASSERT_NON_NULL(arr, return -1);
	DS_ASSERT_NON_NULL(data, return -1);

	return array_set(arr, arr->length, data);
}

int array_addSorted(array * arr, void * data) {

	DS_ASSERT_NON_NULL(arr, return -1);
	DS_ASSERT_NON_NULL(data, return -1);

	return doCompare(arr, data);
}

int array_add(array * arr, void * data) {

	DS_ASSERT_NON_NULL(arr, return -1);
	DS_ASSERT_NON_NULL(data, return -1);

	for (int i = 0; i < arr->length; ++i) {
		if (*((void **)getPtr(i, arr)) == NULL) {
			return array_set(arr, i, data);
		}
	}
	return array_append(arr, data);
}

int array_pop(struct array * arr, void * data_out) {

	int    index  = arr->length -1;
	void * target = getPtr(index, arr);

	memcpy(data_out, target, arr->data_size);

	array_delete(arr, index);
	arr->length -= 1;
	return 0;
}

int array_set(struct array * arr, int index, void * data) {

	DS_ASSERT_NON_NULL(arr, return -1);
	DS_ASSERT_NON_NULL(data, return -1);
	DS_ASSERT_GEQ(index, 0, return -1);

	int length   = arr->length;
	int capacity = arr->capacity;

	if (index < length) {
		if (arr->by_reference) {
			memcpy(getPtr(index, arr), &data, arr->data_size);
		} else {
			memcpy(getPtr(index, arr), data, arr->data_size);
		}
		set(index, arr, 1);
		return index;

	} else if (index < capacity) {
		arr->length = index + 1;
		return array_set(arr, index, data);

	} else if (index >= capacity) {
		inflate(arr);
		return array_set(arr, index, data);
	}

	return -1;
}

static void recurseBandMove(array * arr, int offset, int band) {

	unsigned long bandSize     = getSizeOfBand(band, arr->base_capacity);
	unsigned long prevBandSize = getSizeOfBandsLower(band, arr->base_capacity);
	int           doesOverflow = 0;
	int           lengthOffset = arr->length - prevBandSize;
	int           index = offset + prevBandSize;

	int lastBand = getBandByIndex(arr->length, arr->base_capacity);
	doesOverflow = lastBand > band;

	if (!doesOverflow) {
		if (lengthOffset >= bandSize) {
			doesOverflow = 1;
		}
	}

	if (doesOverflow) {

		char overflowBuffer[arr->data_size + 1];
		memcpy(overflowBuffer, getStatusPtr(bandSize-1+prevBandSize, arr),
				arr->data_size + 1);

		recurseBandMove(arr, 0, band+1);

		memmove(getStatusPtr(index + 1, arr), getStatusPtr(index, arr),
						(arr->data_size + 1)*(bandSize-(offset+1)));

		memcpy(getStatusPtr(bandSize+prevBandSize, arr), overflowBuffer,
				arr->data_size + 1);

	} else {

		memmove(getStatusPtr(index + 1, arr), getStatusPtr(index, arr),
				(arr->data_size + 1)*(bandSize-(offset+1)));
	}
}

int array_insertAt(struct array * arr, int index, void * data) {

	DS_ASSERT_NON_NULL(arr, return -1);
	DS_ASSERT_NON_NULL(data, return -1);
	DS_ASSERT_GEQ(index, 0, return -1);

	if (index < arr->length && index < arr->capacity) {

		int band      = getBandByIndex(index, arr->base_capacity);
		int prevSizes = getSizeOfBandsLower(band, arr->base_capacity);

		if (!isSet(index, arr)) {
			return array_set(arr, index, data);
		}

		if (arr->length + 1 >= arr->capacity) {
			inflate(arr);
		}

		recurseBandMove(arr, index-prevSizes, band);

		arr->length += 1;
		return array_set(arr, index, data);

	} else if (index >= arr->length) {
		return array_set(arr, index, data);
	}

	return -1;
}

void * array_get(struct array * arr, int index) {

	DS_ASSERT_NON_NULL(arr, return NULL);
	DS_ASSERT_GEQ(arr->length, index, return NULL);
	DS_ASSERT_GEQ(index, 0, return NULL);

	if (arr->by_reference) {
		return *((void **)getPtr(index, arr));
	} else {
		return getPtr(index, arr);
	}
}

void array_delete(struct array * arr, int index) {

	DS_ASSERT_NON_NULL(arr, return);
	DS_ASSERT_GEQ(arr->length, index, return);
	DS_ASSERT_GEQ(index, 0, return);

	memset(getPtr(index, arr), 0, arr->data_size);
	set(index, arr, 0);
}

static void recurseBandMoveInverse(array * arr, int offset, int band) {

	unsigned long bandSize     = getSizeOfBand(band, arr->base_capacity);
	unsigned long prevBandSize = getSizeOfBandsLower(band, arr->base_capacity);

	char underflowBuffer[arr->data_size + 1];
	int  index = offset + prevBandSize;

	if (band < arr->data_band_ptr) {
		memcpy(underflowBuffer, getStatusPtr(bandSize+prevBandSize, arr),
				arr->data_size + 1);
	}

	memmove(getStatusPtr(index, arr),
			getStatusPtr(index + 1, arr),
			(arr->data_size + 1)*(bandSize - (offset + 1)));

	if (band >= arr->data_band_ptr) {
		return;
	}

	recurseBandMoveInverse(arr, 0, band+1);

	memcpy(getStatusPtr(prevBandSize+bandSize-1, arr), underflowBuffer,
			arr->data_size + 1);
}

void array_remove(array * arr, int index) {

	DS_ASSERT_NON_NULL(arr, return);
	DS_ASSERT_GEQ(arr->length, index, return);
	DS_ASSERT_GEQ(index, 0, return);

	int band         = getBandByIndex(index, arr->base_capacity);
	int prevBandSize = getSizeOfBandsLower(band, arr->base_capacity);

	memset(getPtr(index, arr), 0, arr->data_size);
	set(index, arr, 0);

	array_delete(arr, index);
	recurseBandMoveInverse(arr, index-prevBandSize, band);

	arr->length -= 1;
}

void array_remove_obj(array * arr, void * obj) {

    DS_ASSERT_NON_NULL(arr, return);
    DS_ASSERT_GEQ(index, 0, return);

    if (arr->comp_f == NULL) {
        for (int i = 0; i < array_getLength(arr); ++i) {
            void* item = array_get(arr, i);
            if (item != NULL && obj == item) {
                array_remove(arr, i);
                break;
            }
        }
    } else {
        int index = array_find(arr, obj);
        if (index >= 0) {
            array_remove(arr, index);
        }
    }
}

int array_find(array * arr, void * data) {

	DS_ASSERT_NON_NULL(arr, return -1);
	DS_ASSERT_NON_NULL(data, return -1);

    if (arr->comp_f != NULL) {
        int    idx     = binarySearchIndex(arr, data);
        int    compVal = 0;
        void * objIdx;

        objIdx  = array_get(arr, idx);
        compVal = arr->comp_f(data, objIdx);
        return idx*(compVal==0) + -1*(compVal!=0);

    } else {
        for (int i = 0; i < arr->length; ++i) {
            void* item = array_get(arr, i);
            if (item != NULL && item == data) {
                return i;
            }
        }
        return -1;
    }

}

int array_getLength(array * arr) {

	DS_ASSERT_NON_NULL(arr, return -1);
	return arr->length;
}

array * array_copy(array * arr) {
	array * new_array = malloc(sizeof(array));
	memcpy(new_array, arr, sizeof(array));

	for (int i = 0; i <= arr->data_band_ptr; ++i) {
		int newBandSize = getSizeOfBand(i, arr->base_capacity);
		new_array->data_bands[i] = malloc(newBandSize*(arr->data_size + 1));
		memcpy(new_array->data_bands[i], arr->data_bands[i], newBandSize*(arr->data_size + 1));
	}

	return new_array;
}
