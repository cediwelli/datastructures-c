#include "buffer.h"
#include "global.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
* Describes how long a Líne-End character-sequence is.
* The CRLF character-sequence is 2 Bytes in length because of \\r\\n.
*/
static int buffer_line_end_length[] = {
	[BUFFER_CR]   = 1,
	[BUFFER_LF]   = 1,
	[BUFFER_CRLF] = 2
};

struct buffer {
	uint32_t mode;
	uint32_t head;
	uint32_t tail;
	uint32_t capacity;
	uint8_t * tape;
};

static void inflate(buffer * b) {
	const int INFLATION_FACTOR = 2;
	int newCapacity = b->capacity*INFLATION_FACTOR;

	b->tape = realloc(b->tape, newCapacity);
	b->capacity = newCapacity;
}

static int isWhite(char c) {
	return (c==' ') || (c=='\t') || (c=='\n') || (c=='\r');
}

static int skipWhite(buffer * b) {
	for (int count = 0; b->tape[b->tail] != '\0'; b->tail += 1) {
		if (!isWhite(b->tape[b->tail])) {
			return count;
		}
		count += 1;
	}
	return -1;
}

static int isLineEnd(uint8_t * bBuffer, buffer_line_end lineEndType) {
	return (bBuffer[0]=='\r' && bBuffer[1]=='\n')*(lineEndType==BUFFER_CRLF) +
		(bBuffer[0]=='\r')*(lineEndType==BUFFER_CR) +
		(bBuffer[0]=='\n')*(lineEndType==BUFFER_LF);
}

static int findLineEnd(uint8_t * bBuffer, int index, size_t n, buffer_line_end lineEndType) {
	for (int endOfLine = 0; endOfLine < n; ++endOfLine) {
		uint8_t * s = &bBuffer[index + endOfLine];
		if (isLineEnd(s, lineEndType) ||
			bBuffer[index + endOfLine] == '\0') {

			return endOfLine;
		}
	}
	return -1;
}

/*static int findNextChar(uint8_t * bBuffer, int index, size_t n, char c) {
	for (int i = 0; i < n; ++i) {
		uint8_t cur = bBuffer[index + i];
		if (cur == c || cur == '\0') {
			return i;
		}
	}
	return n-1;
}*/

static int findNextWhite(uint8_t * bBuffer, int index, size_t n) {
	for (int i = 0; i < n; ++i) {
		uint8_t cur = bBuffer[index + i];
		if (isWhite(cur) || cur == '\0') {
			return i;
		}
	}
	return n-1;
}

static int readStack(buffer * stackBuffer, uint8_t * outBuf, size_t n) {
	if (stackBuffer->head - n >= 0) {
		stackBuffer->head -= n;
		memcpy(outBuf, &stackBuffer->tape[stackBuffer->head], n);
		return n;
	}
	return -1;
}

static int copyOutTape(buffer * tapeBuffer, uint8_t * outBuf, size_t n) {
	if (tapeBuffer->tail + n <= tapeBuffer->head) {
		memcpy(outBuf, &tapeBuffer->tape[tapeBuffer->tail], n);
		tapeBuffer->tail += n;
		return n;
	}
	return -1;
}

/*static int flushTape(buffer * tapeBuffer, uint8_t * outBuf) {
	size_t countLeft = tapeBuffer->head - tapeBuffer->tail;
	if (countLeft > 0 && buffer_read(tapeBuffer, outBuf, countLeft) == -1) {
		return -1;
	}
	return countLeft;
}*/

buffer * buffer_create(int mode) {
	const int START_CAPACITY = 4;
	if (mode == BUFFER_MODE_STACK || mode == BUFFER_MODE_TAPE) {
		buffer * b = (buffer *) malloc(sizeof(buffer));
		b->mode = mode;
		b->head = 0;
		b->tail = 0;
		b->capacity = START_CAPACITY;
		b->tape = (uint8_t *) malloc(START_CAPACITY);
		return b;
	}
	return NULL;
}

void buffer_free(buffer * buffer) {
	if (buffer != NULL) {
		free(buffer->tape);
		free(buffer);
	}
}

void buffer_clear(buffer * buffer, int option) {
	DS_ASSERT_NON_NULL(buffer, return);
	if (option == BUFFER_CLEAR_ALL) {
		buffer->head = 0;
		buffer->tail = 0;
	} else if (buffer_getLength(buffer) > 0) {
		memmove(buffer->tape,
				&buffer->tape[buffer->tail], buffer->head - buffer->tail);
		buffer->head -= buffer->tail;
		buffer->tail = 0;
	}
}

void buffer_rewind(buffer * buffer, int n) {
	DS_ASSERT_NON_NULL(buffer, return);
	if (n == 0 || buffer->tail - n <= 0) {
		buffer->tail = 0;
	} else {
		buffer->tail -= n;
	}
}

int buffer_getLength(buffer * buffer) {
	DS_ASSERT_NON_NULL(buffer, return -1);
	if (buffer->mode == BUFFER_MODE_STACK) {
		return buffer->head;
	} else {
		return buffer->head - buffer->tail;
	}
}

int buffer_getPosition(buffer * buffer) {
	DS_ASSERT_NON_NULL(buffer, return -1);
	if (buffer->mode == BUFFER_MODE_TAPE) {
		return buffer->tail;
	}
	return 0;
}

int buffer_replace(buffer * b, uint8_t old, uint8_t new, int option) {
	DS_ASSERT_NON_NULL(b, return -1);

	int countReplaced = 0;
	int len           = buffer_getLength(b);
	int i             = 0*(option!=BUFFER_LAST) + len*(option==BUFFER_LAST);

	if (option != BUFFER_ALL && option != BUFFER_FIRST &&
			option != BUFFER_LAST) {
		return -1;
	}

	while ((i < len)*(option!=BUFFER_LAST) + (i > 0)*(option==BUFFER_LAST)) {
		if ((countReplaced > 0) &&
				(option == BUFFER_FIRST || option == BUFFER_LAST)) {
			return countReplaced;
		}
		if (b->tape[b->tail + i] == old) {
			b->tape[b->tail + i] = new;
			countReplaced += 1;
		}
		i += 1*(option!=BUFFER_LAST) + -1*(option==BUFFER_LAST);
	}
	return countReplaced;
}

int buffer_skipWhite(buffer * b) {
	DS_ASSERT_NON_NULL(b, return -1);
	return skipWhite(b);
}

int buffer_skip(buffer * b, int amount) {
	DS_ASSERT_NON_NULL(b, return -1);
	DS_ASSERT_GEQ(amount, 1, return -1);

	int len = buffer_getLength(b);

	if (len > amount) {
		b->tail += amount;
		return amount;
	}

	b->tail += len;
	return len;
}

int buffer_write(buffer * buffer, uint8_t * bbuf, size_t n) {

	DS_ASSERT_NON_NULL(buffer, return -1);
	DS_ASSERT_NON_NULL(bbuf, return -1);

	if ((buffer->head + n) >= buffer->capacity) {
		inflate(buffer);
		return buffer_write(buffer, bbuf, n);
	} else {
		memcpy(&buffer->tape[buffer->head], bbuf, n);
		buffer->head += n;
		return n;
	}
	return -1;
}

int buffer_writeInt(buffer * buffer, uint32_t i) {
	DS_ASSERT_NON_NULL(buffer, return -1);
	return buffer_write(buffer, (uint8_t *) &i, sizeof(uint32_t));
}

int buffer_writeLong(buffer * buffer, uint64_t l) {
	DS_ASSERT_NON_NULL(buffer, return -1);
	return buffer_write(buffer, (uint8_t *) &l, sizeof(uint64_t));
}

int buffer_writeFloat(buffer * buffer, float f) {
	DS_ASSERT_NON_NULL(buffer, return -1);
	return buffer_write(buffer, (uint8_t *) &f, sizeof(float));
}

int buffer_writeDouble(buffer * buffer, double d) {
	DS_ASSERT_NON_NULL(buffer, return -1);
	return buffer_write(buffer, (uint8_t *) &d, sizeof(double));
}

int buffer_read(buffer * buffer, uint8_t * bbuf, size_t n) {
	DS_ASSERT_NON_NULL(buffer, return -1);
	if (buffer->mode == BUFFER_MODE_STACK) {
		return readStack(buffer, bbuf, n);
	}
	return copyOutTape(buffer, bbuf, n);
}

int buffer_readLine(buffer * buffer, uint8_t * bbuf, size_t n,
	buffer_line_end line_end) {

	DS_ASSERT_NON_NULL(buffer, return -1);
	DS_ASSERT_EQL(buffer->mode, BUFFER_MODE_TAPE, return -1);

	int bufferlen = buffer_getLength(buffer);
	int maxRead   = (n<=bufferlen)*n + (n>=bufferlen)*bufferlen;
	int eolLength = buffer_line_end_length[line_end];
	int endOfLine = findLineEnd(buffer->tape,
		buffer->tail, maxRead, line_end);

	if (endOfLine != -1) {
		int rv = copyOutTape(buffer, bbuf, endOfLine);
		if (rv != -1) {
			buffer->tail += eolLength;
		}
		return rv;
	}
	return -1;
}

int buffer_readToken(buffer * buffer, uint8_t * bbuf, size_t n) {
	DS_ASSERT_NON_NULL(buffer, return -1);
	DS_ASSERT_NON_NULL(bbuf, return -1);

	int bufferlen = buffer_getLength(buffer);
	int maxRead   = (n<=bufferlen)*n + (n>bufferlen)*bufferlen;
	int tokenLength;

	skipWhite(buffer);
	tokenLength = findNextWhite(buffer->tape, buffer->tail, maxRead);

	if (tokenLength != -1) { // REDUNDANT
		int rv = copyOutTape(buffer, bbuf, tokenLength);
		return rv;
	}
	return -1;
}

int buffer_readInt(buffer * buffer, uint32_t * i) {
	DS_ASSERT_NON_NULL(buffer, return -1);
	DS_ASSERT_NON_NULL(i, return -1);
	return buffer_read(buffer, (uint8_t *) i, sizeof(uint32_t));
}

int buffer_readLong(buffer * buffer, uint64_t * l) {
	DS_ASSERT_NON_NULL(buffer, return -1);
	DS_ASSERT_NON_NULL(l, return -1);
	return buffer_read(buffer, (uint8_t *) l, sizeof(uint64_t));
}

int buffer_readFloat(buffer * buffer, float * f) {
	DS_ASSERT_NON_NULL(buffer, return -1);
	DS_ASSERT_NON_NULL(f, return -1);
	return buffer_read(buffer, (uint8_t *) f, sizeof(float));
}

int buffer_readDouble(buffer * buffer, double * d) {
	DS_ASSERT_NON_NULL(buffer, return -1);
	DS_ASSERT_NON_NULL(d, return -1);
	return buffer_read(buffer, (uint8_t *) d, sizeof(double));
}

uint8_t * buffer_getBuffer(buffer * b) {
	DS_ASSERT_NON_NULL(b, return NULL);
	return &b->tape[b->tail];
}
