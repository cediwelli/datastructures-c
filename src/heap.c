#include "heap.h"

#include "global.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct heap {
	array * heap_array; // array<heap_element *> (reference mode)
	int type;
	size_t size;
};

struct heap_element {
	int priority;
	void * data;
};

static void elementFree(heap * h, heap_element * he) {
	if (h->size != 0) {
		free(he->data);
	}
	free(he);
}

static void upheapify(heap * h) {

	int indexLast = array_getLength(h->heap_array) - 1;
	int indexParent;

	if (indexLast == 0) {
		return;
	}
	indexParent = (indexLast-1)/2;

	while (indexParent >= 0) {
		heap_element * he_last	= array_get(h->heap_array, indexLast);
		heap_element * he_parent= array_get(h->heap_array, indexParent);

		if (
			(h->type==HEAP_MIN)*(he_last->priority < he_parent->priority) +
			(h->type!=HEAP_MIN)*(he_last->priority > he_parent->priority)
		) {
			array_set(h->heap_array, indexParent, he_last);
			array_set(h->heap_array, indexLast, he_parent);
		} else {
			break;
		}

		indexLast	= indexParent;
		indexParent	= (indexLast-1)/2;
	}
}

static void downheapify(heap * h) {

	/* This function could need some simplifications. */

	if(array_getLength(h->heap_array) < 1) {
		return;
	}

	int indexParent     = 0;
	int indexChildLeft  = indexParent*2 + 1;
	int indexChildRight = indexParent*2 + 2;
	int len             = array_getLength(h->heap_array);

	heap_element * parentElement     = array_get(h->heap_array, indexParent);
	heap_element * leftChildElement  = array_get(h->heap_array, indexChildLeft);
	heap_element * rightChildElement = array_get(h->heap_array, indexChildRight);

	while (parentElement != NULL) {
		if (indexParent >= len || indexChildLeft >= len
				|| indexChildRight >= len) {
			break;
		}

		if (
			(h->type==HEAP_MIN)*(parentElement->priority > leftChildElement->priority || parentElement->priority > rightChildElement->priority) +
			(h->type!=HEAP_MIN)*(parentElement->priority < leftChildElement->priority || parentElement->priority < rightChildElement->priority)
		) {
			if (
				(h->type==HEAP_MIN)*(leftChildElement->priority < rightChildElement->priority) +
				(h->type!=HEAP_MIN)*(leftChildElement->priority > rightChildElement->priority)
			) {
				array_set(h->heap_array, indexParent, leftChildElement);
				array_set(h->heap_array, indexChildLeft, parentElement);

				indexParent = indexChildLeft;
			} else {
				array_set(h->heap_array, indexParent, rightChildElement);
				array_set(h->heap_array, indexChildRight, parentElement);

				indexParent = indexChildRight;
			}
		}

		indexChildLeft  = indexParent*2 + 1;
		indexChildRight = indexParent*2 + 2;

		parentElement     = array_get(h->heap_array, indexParent);
		leftChildElement  = array_get(h->heap_array, indexChildLeft);
		rightChildElement = array_get(h->heap_array, indexChildRight);
	}
}

heap * heap_create(size_t size, int type) {

	heap * h = (heap *) malloc(sizeof(heap));

	h->heap_array = array_create(0);
	h->size       = size;
	h->type       = type;
	return h;
}

void heap_free(heap * h) {

	DS_ASSERT_NON_NULL(h, return);

	int len = array_getLength(h->heap_array);

	for (int i = 0; i < len; ++i) {
		elementFree(h, (heap_element *) array_get(h->heap_array, i));
	}
	array_free(h->heap_array);
	free(h);
}

int heap_add(heap * h, void * data, int priority) {

	heap_element * he = (heap_element *) malloc(sizeof(heap_element));

	he->priority = priority;

	if (h->size == 0) {
		he->data = data;
	} else {
		he->data = malloc(h->size);
		memcpy(he->data, data, h->size);
	}

	if (array_append(h->heap_array, he) == -1) {
		if (h->size != 0) {
			free(he->data);
		}
		free(he);
		return -1;
	}
	upheapify(h);
	return 0;
}

int heap_pop(heap * h, void * data_out) {

	DS_ASSERT_NON_NULL(h, return -1);
	DS_ASSERT_NON_NULL(data_out, return -1);

	heap_element * first = array_get(h->heap_array, 0);
	int            len   = array_getLength(h->heap_array);

	if (first == NULL) {
		return -1;
	}

	if (h->size == 0) {
		void ** p_data_out = (void **) data_out;
		*p_data_out = first->data;
	} else {
		memcpy(data_out, first->data, h->size);
	}
	elementFree(h, first);

	if (len > 0) {
		heap_element * last = NULL;
		array_pop(h->heap_array, &last);

		if(len > 1) {
			array_set(h->heap_array, 0, last);
			downheapify(h);
		}
	}
	return 0;
}

int heap_peek(heap * h, void * data_out) {

	DS_ASSERT_NON_NULL(h, return -1);
	DS_ASSERT_NON_NULL(data_out, return -1);

	heap_element * first = array_get(h->heap_array, 0);
	//int            len   = array_getLength(h->heap_array);

	if (first == NULL) {
		return -1;
	}

	if (h->size == 0) {
		void ** p_data_out = (void **) data_out;
		*p_data_out = first->data;
	} else {
		memcpy(data_out, first->data, h->size);
	}
	return 0;
}
