#include "list.h"

#include "global.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct list {
	list_element * front;
	list_element * end;

	int size;
	int by_reference;
};

static list_element * elementCreate(list * l, void * val) {

	list_element * le = (list_element *) malloc(sizeof(list_element));

	if (!l->by_reference) {
		le->value = malloc(l->size);
		memcpy(le->value, val, l->size);
	} else {
		le->value = val;
	}

	le->next = NULL;
	le->prev = NULL;
	return le;
}

list * list_create(size_t size) {

	list * l = (list *) malloc(sizeof(list));

	l->front        = NULL;
	l->end          = NULL;
	l->size         = size;
	l->by_reference	= size==0;
	return l;
}

void list_free(list * list) {

	DS_ASSERT_NON_NULL(list, return);

	for (list_element * it = list->front; it;) {
		list_element * next = it->next;

		if (!list->by_reference)  {
			free(it->value);
		}
		free(it);

		it = next;
	}
	free(list);
}

int list_isEmpty(list * l) {
	return l->front==NULL;
}

void list_append(list * l, void * v) {

	DS_ASSERT_NON_NULL(l, return);

	if (l->front == NULL) {
		l->front = elementCreate(l, v);
		l->end   = l->front;
	} else {
		list_element * le = elementCreate(l, v);

		le->prev     = l->end;
		l->end->next = le;
		l->end       = le;
	}
}

void list_prepend(list * l, void * v) {

	DS_ASSERT_NON_NULL(l, return);

	if (l->front == NULL) {
		list_append(l, v);
	} else {
		list_element * le = elementCreate(l, v);

		le->next       = l->front;
		l->front->prev = le;
		l->front       = le;
	}
}

void list_insert(list * l, int index, void * v) {

	DS_ASSERT_NON_NULL(l, return);

	if (index == 0) {
		list_element * le = elementCreate(l, v);

		le->next = l->front;
		l->front = le;
	} else {
		list_element * cur = l->front;
		list_element * listElement;

		for (int i = 0; i < index-1; cur = cur->next) {
			if(cur->next == NULL) {
				return;
			}
			++i;
		}

		listElement = elementCreate(l, v);

		if (cur->next == NULL) {
			listElement->prev = l->end;
			l->end->next      = listElement;
			l->end            = listElement;
		} else {
			listElement->prev = cur;
			listElement->next = cur->next;
			cur->next         = listElement;
		}
	}
}

int list_popBack(list * l, void * out) {

	DS_ASSERT_NON_NULL(l, return -1);

	list_element * end = l->end;
	void *         obj = NULL;

	if(end != NULL) {
		obj    = end->value;
		l->end = l->end->prev;

		if (l->end != NULL) {
			l->end->next = NULL;
		} else {
			l->front = NULL;
		}

		if (!l->by_reference) {
			memcpy(out, obj, l->size);
			free(end->value);
		} else {
			*((void **) out) = obj;
		}
		free(end);
	}
	return end != NULL;
}

int list_popFront(list * l, void * out) {

	DS_ASSERT_NON_NULL(l, return -1);

	list_element * front = l->front;
	void *         obj   = NULL;

	if (front != NULL) {

		obj      = front->value;
		l->front = l->front->next;

		if (l->front != NULL) {
			l->front->prev = NULL;
		} else {
			l->end = NULL;
		}

		if (!l->by_reference) {
			memcpy(out, obj, l->size);
			free(front->value);
		} else {
			*((void **) out) = obj;
		}
		free(front);
	}
	return front != NULL;
}

void * list_get(list * l, int index) {

	DS_ASSERT_NON_NULL(l, return NULL);

	list_element * cur = l->front;
	int            i   = 0;

	for (; cur->next && i < index; cur = cur->next) {
		++i;
	}
	if (i != index) {
		return NULL;
	}

	return cur->value;
}

void list_remove(list * l, int index) {

	DS_ASSERT_NON_NULL(l, return);

	list_element * cur = l->front;

	if (cur == NULL) {
		return;
	}

	for (int i = 0; i < index; cur = cur->next) {
		if(cur->next == NULL) {
			return;
		}
		++i;
	}

	if (cur->prev) {
		cur->prev->next = cur->next;
	} else {
		l->front = cur->next;
	}

	if (cur->next) {
		cur->next->prev = cur->prev;
	} else {
		l->end = cur->prev;
	}

	if (!l->by_reference) {
		free(cur->value);
	}
	free(cur);
}

list_iterator * list_getIterator(list * l) {
	return l->front;
}
