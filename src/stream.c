#include "stream.h"

#include "global.h"
#include "buffer.h"
#include "array.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct stream {
	uint32_t transmit_amount;

	array * endpoints_in;
	array * endpoints_out;
};

uint32_t stream_fromFD(uint64_t fd, uint8_t * buf, uint32_t n) {
	return read((int)fd, buf, n);
}

uint32_t stream_toFD(uint64_t fd, uint8_t * buf, uint32_t n) {
	return write((int)fd, buf, n);
}

uint32_t stream_fromBuffer(uint64_t fd, uint8_t * buf, uint32_t n) {
	int buflen = buffer_getLength((buffer *)fd);
	int read   = (buflen<n)*buflen + (buflen>=n)*n;

	return buffer_read((buffer *)fd, buf, read);
}

uint32_t stream_toBuffer(uint64_t fd, uint8_t * buf, uint32_t n) {
	return buffer_write((buffer *)fd, buf, n);
}

static int writeOut(endpoint_t * out, uint8_t * buf, uint32_t n) {
	uint32_t head = 0, read = 0;

	while (head < n) {
		read = out->stream_f(out->stream_fd, &buf[head], n - head);
		if (read == -1) {
			return -1;
		} else if (read == 0) {
			break;
		}
		head += read;
	}

	return head;
}

stream * stream_create(uint32_t transmit_amount) {
	stream * p = (stream *) malloc(sizeof(stream));

	memset(p, 0, sizeof(stream));
	p->transmit_amount = transmit_amount;
	p->endpoints_in    = array_create(sizeof(endpoint_t));
	p->endpoints_out   = array_create(sizeof(endpoint_t));
	return p;
}

void stream_free(stream * stream) {
	DS_ASSERT_NON_NULL(stream, return);
	array_free(stream->endpoints_in);
	array_free(stream->endpoints_out);
	free(stream);
}

void stream_setIn(stream * stream, uint64_t in_fd, stream_func f) {
	DS_ASSERT_NON_NULL(stream, return);

	endpoint_t endpoint_in;

	endpoint_in.stream_fd   = in_fd;
	endpoint_in.stream_f = f;
	array_set(stream->endpoints_in, 0, &endpoint_in);
}

void stream_setOut(stream * stream, uint64_t out_fd, stream_func f) {
	DS_ASSERT_NON_NULL(stream, return);

	endpoint_t endpoint_out;

	endpoint_out.stream_fd = out_fd;
	endpoint_out.stream_f  = f;
	array_set(stream->endpoints_out, 0, &endpoint_out);
}

int stream_addIn(stream * stream, uint64_t in_fd, stream_func f) {
	DS_ASSERT_NON_NULL(stream, return -1);

	endpoint_t e;

	e.stream_fd = in_fd;
	e.stream_f  = f;
	return array_add(stream->endpoints_in, &e);
}

void stream_remIn(stream * stream, int idx) {
	DS_ASSERT_NON_NULL(stream, return);

	array_delete(stream->endpoints_in, idx);
}

int stream_addOut(stream * stream, uint64_t out_fd, stream_func f) {
	DS_ASSERT_NON_NULL(stream, return -1);

	endpoint_t e;

	e.stream_fd = out_fd;
	e.stream_f  = f;
	return array_add(stream->endpoints_out, &e);
}

void stream_remOut(stream * stream, int idx) {
	DS_ASSERT_NON_NULL(stream, return);

	array_delete(stream->endpoints_in, idx);
}

uint32_t stream_transmit(stream * stream, uint32_t * amounts) {

	int countEndpointsOut = array_getLength(stream->endpoints_out);
	int countEndpointsIn  = array_getLength(stream->endpoints_in);

	/* Read from all IN-Endpoints and write to all OUT-Endpoints */

	for (int i = 0; i < countEndpointsIn; ++i) {
		uint8_t      intermediateBuffer[stream->transmit_amount];
		endpoint_t * endpoint_in = array_get(stream->endpoints_in, i);

		if (endpoint_in == NULL) {
			continue;
		}

		uint32_t readAmount = endpoint_in->stream_f(
				endpoint_in->stream_fd, intermediateBuffer,
				stream->transmit_amount);

		if (readAmount == -1) {
			continue;
		}

		for (int k = 0; k < countEndpointsOut; ++k) {
			endpoint_t * endpoint_out = array_get(stream->endpoints_out, k);

			if (endpoint_out != NULL) {
				int rv;
				if ((rv = writeOut(endpoint_out, intermediateBuffer, readAmount)) == -1) {
					// Handle errors?
				}
				if (amounts != NULL) {
					amounts[k] = rv;
				}
			}
		}
	}

	return 0;
}

uint32_t stream_pump(endpoint_t * in, endpoint_t * out, int n) {
	uint8_t buffer[n];

	n = in->stream_f(in->stream_fd, buffer, n);

	while (n > 0) {
		int k = out->stream_f(out->stream_fd, buffer, n);
		if (k == -1) {
			return -1;
		}
		if (k == 0) {
			break;
		}
		n -= k;
	}
	return n;
}
