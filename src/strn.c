#include "strn.h"
#include "global.h"

#include <string.h>
#include <stdlib.h>

struct strn {
    char * buffer;
    int   length;
    int   allocated_length;
};

static void resize(strn * s) {
    char * new_buffer;
    int    new_length = s->allocated_length*2;

    new_buffer          = malloc(new_length);
    s->allocated_length = new_length;

    strcpy(new_buffer, s->buffer);
    free(s->buffer);
    s->buffer = new_buffer;
}

strn * strn_create() {
    const int start_length = 8;
    strn *    s            = malloc(sizeof(strn));

    s->buffer           = malloc(start_length);
    s->length           = 0;
    s->allocated_length = start_length;
    memset(s->buffer, 0, start_length);
    return s;
}

void strn_free(strn * s) {
    DS_ASSERT_NON_NULL(s, return);

    free(s->buffer);
    free(s);
}

int strn_set(strn * s, char * str) {
    int str_length;

    DS_ASSERT_NON_NULL(s, return -1);
    DS_ASSERT_NON_NULL(str, return -1);

    str_length = strlen(str);
    if (str_length > s->allocated_length) {
        resize(s);
    }
    strcpy(s->buffer, str);
    return 0;
}

int strn_append(strn * s, char * str) {
    int str_length;

    DS_ASSERT_NON_NULL(s, return -1);
    DS_ASSERT_NON_NULL(str, return -1);

    str_length = strlen(str);
    while (str_length > s->allocated_length) {
        resize(s);
    }
    strcat(s->buffer, str);
    return 0;
}

char * strn_getString(strn * s) {
    DS_ASSERT_NON_NULL(s, return NULL);
    return s->buffer;
}