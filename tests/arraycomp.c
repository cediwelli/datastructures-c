#include <datastructures/array.h>
#include <criterion/criterion.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int comp_int(void* obj1, void* obj2) {
	if (obj1 == NULL || obj2 == NULL) {
		return -2;
	}
	int i1 = *((int*)obj1);
	int i2 = *((int*)obj2);

	cr_log_info("Testing (%d <=> %d) is %d", i1, i2, (i1<i2)*-1 + (i1>i2)*1 + (i1==i2)*0);
	return (i1<i2)*-1 + (i1>i2)*1 + (i1==i2)*0;
}

int comp_str(void* obj1, void* obj2) {
	if (obj1 == NULL || obj2 == NULL) {
		return -2;
	}
	char* s1 = (char*)obj1;
	char* s2 = (char*)obj2;
	int res = strcmp(s1, s2);
	cr_log_info("Testing (%s <=> %s) is %d", s1, s2, res);
	//sleep(1);
	return (res<0)*-1 + (res>0)*1 + (res==0)*0;
}

/*
Test(arraycomp, add_int_asc_ss1) {
	array* arr = array_create(sizeof(int));
	array_setCompMode(arr, comp_int);

	const int MAX = 1000;

	for (int i = 0; i < MAX; ++i) {
		array_addSorted(arr, &i);
	}

	for (int i = 0; i < MAX; ++i) {
		int* i1 = (int*) array_get(arr, i);
		cr_assert(*i1 == i, "Values don't match (%d != %d)", *i1, i);
	}
}

Test(arraycomp, add_int_asc_ss2) {
	array* arr = array_create(sizeof(int));
	array_setCompMode(arr, comp_int);

	const int MAX = 1000;

	for (int i = 0; i < MAX; i += 2) {
		array_addSorted(arr, &i);
	}

	for (int i = 0; i < MAX; i += 2) {
		int* i1 = (int*) array_get(arr, i/2);
		cr_assert(*i1 == i, "Values don't match (%d != %d)", *i1, i);
	}
}

Test(arraycomp, add_int_dsc_ss1) {
	array* arr = array_create(sizeof(int));
	array_setCompMode(arr, comp_int);

	const int MAX = 1000;

	for (int i = MAX; i >= 0; --i) {
		array_addSorted(arr, &i);
	}

	for (int i = 0; i < MAX; ++i) {
		int* i1 = (int*) array_get(arr, i);
		cr_assert(*i1 == i, "Values don't match (%d != %d)", *i1, i);
	}
}
*/
Test(arraycomp, add_int_cstm1) {
	array* arr = array_create(sizeof(int));
	array_setCompMode(arr, comp_int);

	int dataset[]  = {10, 4, 3, 2, -4, 4, 0, 65, 64, 63, 65};
	int expected[] = {-4, 0, 2, 3, 4, 4, 10, 63, 64, 65, 65};

	for (int i = 0; i < sizeof(dataset)/sizeof(dataset[0]); ++i) {
		array_addSorted(arr, &dataset[i]);
	}

	for (int i = 0; i < sizeof(expected)/sizeof(expected[0]); ++i) {
		int* i1 = (int*) array_get(arr, i);
		cr_log_info("%d ?= %d", *i1, expected[i]);
	}

	for (int i = 0; i < sizeof(expected)/sizeof(expected[0]); ++i) {
		int* i1 = (int*) array_get(arr, i);
		cr_assert(*i1 == expected[i], "Values don't match (%d != %d)", *i1, expected[i]);
	}
}

Test(arraycomp, add_int_cstm2) {
	array* arr = array_create(sizeof(int));
	array_setCompMode(arr, comp_int);

	int dataset[]  = {10, 4, 3, 2, -4, 4, 0, 65, 64, 63, 65, 66, 66, 67};
	int expected[] = {-4, 0, 2, 3, 4, 4, 10, 63, 64, 65, 65, 66, 66, 67};

	for (int i = 0; i < sizeof(dataset)/sizeof(dataset[0]); ++i) {
		array_addSorted(arr, &dataset[i]);
	}

	for (int i = 0; i < sizeof(expected)/sizeof(expected[0]); ++i) {
		int* i1 = (int*) array_get(arr, i);
		cr_log_info("%d ?= %d", *i1, expected[i]);
	}

	for (int i = 0; i < sizeof(expected)/sizeof(expected[0]); ++i) {
		int* i1 = (int*) array_get(arr, i);
		cr_assert(*i1 == expected[i], "Values don't match (%d != %d)", *i1, expected[i]);
	}
}

Test(arraycomp, add_str_cstm1) {
	array* arr = array_create(0);
	array_setCompMode(arr, comp_str);

	char* dataset[] = {
		"Host",
		"User-Agent",
		"Accept",
		"Accept-Language",
		"Accept-Encoding",
		"Referer",
		"Alt-Used",
		"Connection",
		"Cookie",
		"Sec-Fetch-Dest",
		"Sec-Fetch-Mode",
		"Sec-Fetch-Site",
		"Pragma",
		"Cache-Control",
		"TE"
	};

	char* expected[] = {
		"Accept",
		"Accept-Encoding",
		"Accept-Language",
		"Alt-Used",
		"Cache-Control",
		"Connection",
		"Cookie",
		"Host",
		"Pragma",
		"Referer",
		"Sec-Fetch-Dest",
		"Sec-Fetch-Mode",
		"Sec-Fetch-Site",
		"TE",
		"User-Agent"
	};

	for (int i = 0; i < 15; ++i) {
		array_addSorted(arr, dataset[i]);
	}


	for (int i = 0; i < sizeof(expected)/sizeof(expected[0]); ++i) {
		char* s1 = (int*) array_get(arr, i);
		cr_log_info("%s ?= %s", s1, expected[i]);
	}

	for (int i = 0; i < sizeof(expected)/sizeof(expected[0]); ++i) {
		char* s1 = (int*) array_get(arr, i);
		cr_assert(strcmp(s1,expected[i])==0, "Values don't match (%s != %s)", s1, expected[i]);
	}
}
/*
 * Host
User-Agent
Accept
Accept-Language
Accept-Encoding
Referer
Alt-Used
Connection
Cookie
Sec-Fetch-Dest
Sec-Fetch-Mode
Sec-Fetch-Site
Pragma
Cache-Control
TE
 * */