#include <criterion/criterion.h>
#include <stdio.h>
#include <stdlib.h>

#include <time.h>

#include <datastructures/array.h>

void setup(void) {
	srand(time(0));
}

static int getRand(int max, int min) {
	return (rand()%(max-min))+min;
}

static int comp_int(void * obj1, void * obj2) {

	int * iobj1 = (int *) obj1;
	int * iobj2 = (int *) obj2;

	if ((obj1 == NULL || obj2 == NULL)) {
		return -2;
	}

	return (*iobj1<*iobj2)*-1 + (*iobj1>*iobj2)*1 + 0;
}

Test(arraytest, Create_reference) {
	array * arr = array_create(0);
	cr_assert(arr != NULL, "Array could not be created!");
	array_free(arr);
}

Test(arraytest, Create_value) {
	array * arr = array_create(sizeof(int));
	cr_assert(arr != NULL, "Array could not be created!");
	array_free(arr);
}

/**
 * REFERENCE MODE
 * Tests the Setting (and also Getting) of Elements in the Array.
 * In this Test, an Element is only written to once and they are added in order.
 * */
Test(arraytest, SetAndGet_reference, .init=setup) {

	array * arr = array_create(0);
	int     len = 10000;
	int     inp[len];

	memset(inp, 0, len*sizeof(int));

	cr_log_info("Priming input-array with random values...");
	for (int i = 0; i < len; ++i) {
		int n = getRand(500, -500);

		inp[i] = n;
		array_set(arr, i, &inp[i]);
	}

	cr_log_info("Getting from array...");
	for (int i = 0; i < len; ++i) {
		int * pn = array_get(arr, i);

		cr_assert(pn != NULL, "array_get() returned NULL at %d.", i);
		cr_assert(*pn == inp[i], "Values do not match after array_get():"
				" %d != %d @ %d.", *pn, inp[i], i);
	}

	array_free(arr);
}

/**
 * VALUE MODE
 * Tests the Setting (and also Getting) of Elements in the Array.
 * In this Test, an Element is only written to once and they are added in order.
 * */
Test(arraytest, SetAndGet_value, .init=setup) {

	array * arr = array_create(sizeof(int));
	int     len = 10000;
	int     inp[len];

	memset(inp, 0, len*sizeof(int));

	cr_log_info("Priming input-array with random values...");
	for (int i = 0; i < len; ++i) {
		int n = getRand(500, -500);

		inp[i] = n;
		array_set(arr, i, &n);
	}

	cr_log_info("Getting from array...");
	for (int i = 0; i < len; ++i) {
		int * pn = array_get(arr, i);

		cr_assert(pn != NULL, "array_get() returned NULL at %d.", i);
		cr_assert(*pn == inp[i], "Values do not match after array_get():"
				" %d != %d @ %d.", *pn, inp[i], i);
	}

	array_free(arr);
}

/**
 * REFERENCE MODE
 * Tests for correct array_get() output in Value-Mode.
 * This time with random input.
 * */
Test(arraytest, AppendAndGet_reference, .init=setup) {

	array * arr = array_create(0);
	int     len = getRand(100, 50);
	int     inp[len];

	cr_log_info("Priming input-array with random values...");
	for (int i = 0; i < len; ++i) {
		inp[i] = getRand(10000, -10000);
	}

	cr_log_info("Appending to array...");
	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int rv = array_append(arr, &inp[i]);
		cr_assert(rv != -1, "array_append() failed on iteration %d with value "
				"%d.", i, inp[i]);
	}

	cr_log_info("Getting from array...");
	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int * rv = array_get(arr, i);
		cr_assert(rv != NULL, "array_get() returned NULL!");
		cr_assert(*rv == inp[i], "Values do not match!");
	}

	array_free(arr);
}

/**
 * VALUE MODE
 * Tests for correct array_get() output in Value-Mode.
 * This time with random input.
 * */
Test(arraytest, AppendAndGet_value, .init=setup) {

	array * arr = array_create(sizeof(int));
	int     len = getRand(100, 50);
	int     inp[len];

	cr_log_info("Priming input-array with random values...");
	for (int i = 0; i < len; ++i) {
		inp[i] = getRand(10000, -10000);
	}

	cr_log_info("Appending to array...");
	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int rv = array_append(arr, &inp[i]);
		cr_assert(rv != -1, "array_append() failed on iteration %d with value "
				"%d.", i, inp[i]);
	}

	cr_log_info("Getting from array...");
	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int * rv = array_get(arr, i);
		cr_assert(rv != NULL, "array_get() returned NULL!");
		cr_assert(*rv == inp[i], "Values do not match!");
	}

	array_free(arr);
}

Test(arraytest, Insertion) {
	array * arr   = array_create(sizeof(int));
	int     inp[] = {5, 3, 16, -1, 11, 7, 54, 32, 6, 100};
	int     ind[] = {0, 1, 2,  0,  1, 2,  5,  3, 0,   4};
	//int     ind[] = {9, 8, 7,  6,  5, 4,  3,  2, 1,   0};
	int     exp[] = {6, -1, 11, 7, 100, 32, 5, 3, 54, 16};
	//int     ind[] = {3, 3, 3,  3,  3, 3,  3,  3, 3,   3};
	//int     exp[] = {6, -1, 11, 7, 100, 32, 5, 3, 54, 0};

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int rv = array_insertAt(arr, ind[i], &inp[i]);
	}

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int * i_ret = array_get(arr, i);
		cr_assert(*i_ret == exp[i], "%d != %d at %d", *i_ret, exp[i], i);
	}
}

Test(arraytest, SortedInsert) {

	array * arr   = array_create(sizeof(int));
	int     inp[] = {5, 3, 0, -1, 11, 7, 54, 32, 6, 100};
	int     exp[] = {-1, 0, 3, 5, 6, 7, 11, 32, 54, 100};

	array_setCompMode(arr, comp_int);

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int rv = array_addSorted(arr, &inp[i]);
		cr_assert(rv != -1);

		for (int i = 0; i < array_getLength(arr); ++i) {
			int * i_ret = array_get(arr, i);
			/*if (i_ret != NULL)
				printf(": %d\n", *i_ret);
			else
				printf(": NULL\n");*/
		}
	}

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int * i_ret = array_get(arr, i);
		/*if (i_ret != NULL)
			printf(": %d\n", *i_ret);
		else
			printf(": NULL\n");*/
		cr_assert(*i_ret == exp[i], "%d != %d at %d", *i_ret, exp[i], i);
	}
}

Test(arraytest, SortedInsert2) {

	array * arr   = array_create(sizeof(int));
	int     inp[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	int     exp[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	array_setCompMode(arr, comp_int);

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int rv = array_addSorted(arr, &inp[i]);
		cr_assert(rv != -1);

		for (int i = 0; i < array_getLength(arr); ++i) {
			int * i_ret = array_get(arr, i);
			/*if (i_ret != NULL)
				printf(": %d\n", *i_ret);
			else
				printf(": NULL\n");*/
		}
	}

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int * i_ret = array_get(arr, i);
		/*if (i_ret != NULL)
			printf(": %d\n", *i_ret);
		else
			printf(": NULL\n");*/
		cr_assert(*i_ret == exp[i], "%d != %d at %d", *i_ret, exp[i], i);
	}
}

Test(arraytest, SortedInsert3) {

	array * arr   = array_create(sizeof(int));
	int     inp[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
	int     exp[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	array_setCompMode(arr, comp_int);

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int rv = array_addSorted(arr, &inp[i]);
		cr_assert(rv != -1);

		for (int i = 0; i < array_getLength(arr); ++i) {
			int * i_ret = array_get(arr, i);
			/*if (i_ret != NULL)
				printf(": %d\n", *i_ret);
			else
				printf(": NULL\n");*/
		}
	}

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int * i_ret = array_get(arr, i);
		/*if (i_ret != NULL)
			printf(": %d\n", *i_ret);
		else
			printf(": NULL\n");*/
		cr_assert(*i_ret == exp[i], "%d != %d at %d", *i_ret, exp[i], i);
	}
}

Test(arraytest, Find) {

	array * arr   = array_create(0);
	int     inp[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
	int     idx;
	int     d     = 11;

	array_setCompMode(arr, comp_int);

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int rv = array_addSorted(arr, &inp[i]);
		cr_assert(rv != -1);
	}

	idx = array_find(arr, &inp[0]);
	cr_assert(idx == 9, "%d != %d", idx, 9);

	idx = array_find(arr, &inp[1]);
	cr_assert(idx == 8);

	idx = array_find(arr, &d);
	cr_assert(idx == -1);
}

Test(arraytest, Remove1) {

	array * arr   = array_create(sizeof(int));
	int     inp[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
	int     exp[] = {10, 9, 7, 6, 5, 4, 3, 2, 1};

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int rv = array_append(arr, &inp[i]);
		cr_assert(rv != -1);
	}

	array_remove(arr, 2);

	for (int i = 0; i < array_getLength(arr); ++i) {
		int * v = array_get(arr, i);
		cr_assert(*v == exp[i], "%d != %d", *v, exp[i]);
	}
}

Test(arraytest, Remove2) {

	array * arr   = array_create(sizeof(int));
	int     inp[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
	int     exp[] = {9, 8, 7, 6, 5, 4, 3, 2, 1};

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int rv = array_append(arr, &inp[i]);
		cr_assert(rv != -1);
	}

	array_remove(arr, 0);

	for (int i = 0; i < array_getLength(arr); ++i) {
		int * v = array_get(arr, i);
		cr_assert(*v == exp[i], "%d != %d", *v, exp[i]);
	}
}

Test(arraytest, Copy) {
	array * arr   = array_create(sizeof(int));
	int     inp[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 19, 29, 0, 0, 0, 4, 21, 3, 1};

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		array_append(arr, &inp[i]);
	}

	array * arr2 = array_copy(arr);

	for (int i = 0; i < array_getLength(arr2); ++i) {
		int * val1 = array_get(arr, i);
		int * val2 = array_get(arr2, i);
		cr_assert(*val1 == *val2);
	}
}
