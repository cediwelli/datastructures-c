#include <criterion/criterion.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <datastructures/buffer.h>

Test(buffertest, SkipWhite) {
	buffer * buffer = buffer_create(BUFFER_MODE_TAPE);
	char     text[]     = "    Dies ist ein Text!";
	char     expected[] = "Dies ist ein Text!";
	char     out[sizeof(text)];
	int      rv;

	buffer_write(buffer, text, sizeof(text));
	rv = buffer_skipWhite(buffer);
	buffer_read(buffer, out, sizeof(text) - rv);

	cr_assert_str_eq(out, expected);
}

Test(buffertest, ReadTooMuch) {
	buffer * buffer = buffer_create(BUFFER_MODE_TAPE);
	char     text[] = "Dies ist ein Text!";
	char     out[sizeof(text)];
	int      rv;

	buffer_write(buffer, text, sizeof(text));
	rv = buffer_read(buffer, out, 4);
	cr_assert(rv == 4);
	rv = buffer_read(buffer, out, sizeof(text));
	cr_assert(rv == -1);
}

Test(buffertest, ReadToken) {
	buffer * buffer     = buffer_create(BUFFER_MODE_TAPE);
	char     text[]     = "Dies   ist      ein\t\n  Test mit Tokens!";
	char *   expected[] = {"Dies", "ist", "ein", "Test", "mit", "Tokens!"};

	buffer_write(buffer, text, sizeof(text));

	for (int i = 0; i < sizeof(expected)/sizeof(char*); ++i) {
		char token[256];

		int rv = buffer_readToken(buffer, token, 255);
		cr_assert(rv != -1, "readToken returned -1");

		token[rv] = '\0';
		cr_assert_str_eq(token, expected[i], "Strings are not equal!");
	}

	buffer_free(buffer);
}

Test(buffertest, ReplaceFirst) {
	buffer * buffer     = buffer_create(BUFFER_MODE_TAPE);
	char     text[]     = "Dies ist ein Test mit Tokens!";
	char     expected[] = "DXes ist ein Test mit Tokens!";
	char     out[sizeof(text)];
	int      rv;

	buffer_write(buffer, text, sizeof(text));
	rv = buffer_replace(buffer, 'i', 'X', BUFFER_FIRST);
	buffer_read(buffer, out, sizeof(text));

	cr_assert(rv == 1);
	cr_assert_str_eq(out, expected);
	buffer_free(buffer);
}

Test(buffertest, ReplaceLast) {
	buffer * buffer     = buffer_create(BUFFER_MODE_TAPE);
	char     text[]     = "Dies ist ein Test mit Tokens!";
	char     expected[] = "Dies ist ein Test mXt Tokens!";
	char     out[sizeof(text)];
	int      rv;

	buffer_write(buffer, text, sizeof(text));
	rv = buffer_replace(buffer, 'i', 'X', BUFFER_LAST);
	buffer_read(buffer, out, sizeof(text));

	cr_assert(rv == 1);
	cr_assert_str_eq(out, expected);
	buffer_free(buffer);
}

Test(buffertest, ReplaceAll) {
	buffer * buffer     = buffer_create(BUFFER_MODE_TAPE);
	char     text[]     = "Dies ist ein Test mit Tokens!";
	char     expected[] = "DXes Xst eXn Test mXt Tokens!";
	char     out[sizeof(text)];
	int      rv;

	buffer_write(buffer, text, sizeof(text));
	rv = buffer_replace(buffer, 'i', 'X', BUFFER_ALL);
	buffer_read(buffer, out, sizeof(text));

	cr_assert(rv == 4);
	cr_assert_str_eq(out, expected);
	buffer_free(buffer);
}

Test(buffertest, ReplaceAllDifferentStart) {
	buffer * buffer     = buffer_create(BUFFER_MODE_TAPE);
	char     text[]     = "Dies ist ein Test mit Tokens!";
	char     expected[] = "Dies Xst eXn Test mXt Tokens!";
	char     out[sizeof(text)];
	int      rv;

	buffer_write(buffer, text, sizeof(text));
	buffer_read(buffer, out, 4);

	rv = buffer_replace(buffer, 'i', 'X', BUFFER_ALL);
	buffer_rewind(buffer, 0);
	buffer_read(buffer, out, sizeof(text));

	cr_assert(rv == 3);
	cr_assert_str_eq(out, expected);
	buffer_free(buffer);
}
