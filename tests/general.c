
#include <datastructures/array.h>
#include <stdlib.h>
#include <stdio.h>

int main() {

	array * arr   = array_create(sizeof(int));
	int     inp[] = {5, 3, 0, -1, 11, 7, 54, 32, 6, 100};
	int     ind[] = {0, 1, 2,  0,  1, 2,  5,  3, 0,   4};
	int     exp[] = {6, -1, 11, 7, 100, 32, 5, 3, 54, 0};
	//int     ind[] = {3, 3, 3,  3,  3, 3,  3,  3, 3,   3};
	//int     exp[] = {6, -1, 11, 7, 100, 32, 5, 3, 54, 0};

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int rv = array_insertAt(arr, ind[i], &inp[i]);
		//printf("added: %d\n", array_append(arr, &inp[i]));
	}

	for (int i = 0; i < sizeof(inp)/sizeof(int); ++i) {
		int * i_ret = array_get(arr, i);
		printf("%d: %d ?= %d\n", i, *i_ret, exp[i]);
		//cr_assert(*i_ret == exp[i], "%d != %d at %d", *i_ret, exp[i], i);
		/*if (*i_ret != inp[i]) {
			printf("ERROR %d: %d != %d\n", i, *i_ret, inp[i]);
			exit(0);
		}*/
	}

}
