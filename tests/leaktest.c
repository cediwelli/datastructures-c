#include <criterion/criterion.h>
#include <stdio.h>
#include <stdlib.h>

#include <datastructures/array.h>

Test(leaktest, leakarray_value) {
	array * arr = array_create(sizeof(int));

	for (int i = 0; i < 100; ++i) {
		array_append(arr, &i);
	}

	array_free(arr);
}
