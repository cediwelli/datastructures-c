#include <criterion/criterion.h>
#include <stdio.h>
#include <stdlib.h>

#include <datastructures/list.h>

Test(listtest, Create) {
	list * list = list_create(0);
	cr_assert(list != NULL);
	list_free(list);
}

Test(listtest, AppendSmallReference) {
	list * list   = list_create(0);
	int    data[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

	for (int i = 0; i < sizeof(data)/sizeof(int); ++i) {
		list_append(list, &data[i]);
	}

	// This is supposed to be get and not make use of iterator
	for (int i = 0; i < sizeof(data)/sizeof(int); ++i) {
		int * v = list_get(list, i);
		cr_assert(data[i] == *v);
		cr_assert(&data[i] == v);
	}

	list_free(list);
}

Test(listtest, AppendSmallValue) {
	list * list   = list_create(sizeof(int));
	int    data[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

	for (int i = 0; i < sizeof(data)/sizeof(int); ++i) {
		list_append(list, &data[i]);
	}

	// This is supposed to be get and not make use of iterator
	for (int i = 0; i < sizeof(data)/sizeof(int); ++i) {
		int * v = list_get(list, i);
		cr_assert(data[i] == *v);
	}

	list_free(list);
}

Test(listtest, PopBackReference) {
	list * list = list_create(0);
	int    data[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	int    rv;
	int *  back;

	for (int i = 0; i < sizeof(data)/sizeof(int); ++i) {
		list_append(list, &data[i]);
	}

	rv = list_popBack(list, &back);
	cr_assert(rv == 1);
	cr_assert(*back == data[8]);

	list_free(list);
}

Test(listtest, PopBackValue) {
	list * list = list_create(sizeof(int));
	int    data[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	int    rv;
	int    back;

	for (int i = 0; i < sizeof(data)/sizeof(int); ++i) {
		list_append(list, &data[i]);
	}

	rv = list_popBack(list, &back);
	cr_assert(rv == 1);
	cr_assert(back == data[8]);

	list_free(list);
}

Test(listtest, PopBackAllValue) {
	list * list = list_create(sizeof(int));
	int    data[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	int    exp[]  = {9, 8, 7, 6, 5, 4, 3, 2, 1};
	int    i      = 0;
	int    back;

	for (int i = 0; i < sizeof(data)/sizeof(int); ++i) {
		list_append(list, &data[i]);
	}

	cr_assert(list_isEmpty(list) == 0,);
	while (list_popBack(list, &back)) {
		cr_assert(back == exp[i++], "%d != %d", back, exp[i]);
	}
	cr_assert(i == 9);
	cr_assert(list_isEmpty(list) == 1);

	list_free(list);
}

Test(listtest, PrependFilledValue) {
	list * list       = list_create(sizeof(int));
	int    data[]     = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	int    prependObj = 1889;
	int    i          = 0;
	int    rv;

	for (int i = 0; i < sizeof(data)/sizeof(int); ++i) {
		list_append(list, &data[i]);
	}
	list_prepend(list, &prependObj);

	for (list_iterator * it = list_getIterator(list); it; it = it->next) {
		int * val = (int *) it->value;
		if (i == 0) {
			cr_assert(*val == prependObj);
		} else {
			cr_assert(*val == data[i-1]);
		}

		++i;
	}
}

Test(listtest, PrependEmptyValue) {
	list * list       = list_create(sizeof(int));
	int    data[]     = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	int    prependObj = 1889;
	int    i          = 0;
	int    rv;

	list_prepend(list, &prependObj);
	i = *((int *)list_get(list, 0));

	cr_assert(i == prependObj);
}

Test(listtest, PrependAllToEmptyValue) {
	list * list       = list_create(sizeof(int));
	int    data[]     = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	int    exp[]      = {9, 8, 7, 6, 5, 4, 3, 2, 1};
	int    i          = 0;
	int    rv;

	for (int i = 0; i < sizeof(data)/sizeof(int); ++i) {
		list_prepend(list, &data[i]);
	}

	for (list_iterator * it = list_getIterator(list); it; it = it->next) {
		int * val = (int *) it->value;

		cr_assert(*val == exp[i]);
		++i;
	}
}
