#include <criterion/criterion.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <datastructures/buffer.h>
#include <datastructures/stream.h>

Test(streamtest, 1To1) {

	buffer * b_in  = buffer_create(BUFFER_MODE_TAPE);
	buffer * b_out = buffer_create(BUFFER_MODE_TAPE);
	stream * s = stream_create(1024);
	char     d[] = "Test String Hello World";
	char     o[sizeof(d)];
	int      rv;

	stream_addIn(s, (uint64_t)b_in, stream_fromBuffer);
	stream_addOut(s, (uint64_t)b_out, stream_toBuffer);

	rv = buffer_write(b_in, d, sizeof(d));
	stream_transmit(s, NULL);
	buffer_read(b_out, o, sizeof(d));

	cr_assert_str_eq(o, d);

	buffer_free(b_in);
	buffer_free(b_out);
	stream_free(s);
}

Test(streamtest, 1To2) {

	buffer * b_in  = buffer_create(BUFFER_MODE_TAPE);
	buffer * b_out1 = buffer_create(BUFFER_MODE_TAPE);
	buffer * b_out2 = buffer_create(BUFFER_MODE_TAPE);
	stream * s = stream_create(1024);
	char     d[] = "Test String Hello World";
	char     o[sizeof(d)];
	int      rv;

	stream_addIn(s, (uint64_t)b_in, stream_fromBuffer);
	stream_addOut(s, (uint64_t)b_out1, stream_toBuffer);
	stream_addOut(s, (uint64_t)b_out2, stream_toBuffer);

	rv = buffer_write(b_in, d, sizeof(d));
	stream_transmit(s, NULL);

	buffer_read(b_out1, o, sizeof(d));
	cr_assert_str_eq(o, d);

	memset(o, 0, sizeof(d));

	buffer_read(b_out2, o, sizeof(d));
	cr_assert_str_eq(o, d);
}
